<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_allocationform;

/**
 * Defines important dates that will be displayed to a user.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2022 onwards, University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class dates extends \core\activity_dates {
    /**
     * Returns a list of important dates for this module.
     *
     * @return array[] Each element of the array is an array with keys:
     *                 label - The label for the date
     *                 timestamp - The date
     */
    protected function get_dates(): array {
        if ($this->cm->customdata['state'] == helper::STATE_EDITING) {
            // Do not display dates when the form is in the editing state, since it cannot be available to students.
            return [];
        }

        $timeopen = $this->cm->customdata['startdate'] ?? null;
        $timeclose = $this->cm->customdata['deadline'] ?? null;
        $now = time();
        $dates = [];

        $openlabelid = $timeopen > $now ? 'activitydate:opens' : 'activitydate:opened';
        $dates[] = [
            'label' => get_string($openlabelid, 'course'),
            'timestamp' => (int) $timeopen,
        ];

        $closelabelid = $timeclose > $now ? 'activitydate:closes' : 'activitydate:closed';
        $dates[] = [
            'label' => get_string($closelabelid, 'mod_allocationform'),
            'timestamp' => (int) $timeclose,
        ];

        return $dates;
    }
}
