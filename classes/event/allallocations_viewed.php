<?php
// This file is part of the allocation form activity module
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing allallocations_viewed class
 *
 * @package    mod_allocationform
 * @copyright  2015 onwards University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk> - Upgrade to Moodle 2.7
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\event;

/**
 * Stores an "all allocation viewed" event
 *
 * @package    mod_allocationform
 * @copyright  2015 onwards University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk> - Upgrade to Moodle 2.7
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class allallocations_viewed extends \core\event\course_module_instance_list_viewed {
    // No need for any code here as everything is handled by the parent class.
}
