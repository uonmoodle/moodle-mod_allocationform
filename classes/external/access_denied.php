<?php
// This file is part of the allocation form plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_allocationform\external;
use core_external\external_function_parameters;
use core_external\external_value;
use core_external\external_warnings;

/**
 * Logs that a user was denied access to view a Allocation form activity.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class access_denied extends \core_external\external_api {
    /**
     * Logs that a user viewed an Allocation form activity.
     *
     * @param int $id The id of a allocation form activity.
     * @return array
     */
    public static function view($id) {
        global $DB;
        $params = self::validate_parameters(self::view_parameters(), ['id' => $id]);
        // Get the allocation form activity and check that the user should have access to it.
        list($course, $cm) = get_course_and_cm_from_instance($params['id'], 'allocationform');
        $context = \context_module::instance($cm->id);
        self::validate_context($context);

        $status = true;
        $warnings = [];

        $eventparams = [
            'objectid' => $cm->instance,
            'context' => $context,
        ];
        $event = \mod_allocationform\event\access_denied::create($eventparams);
        $event->trigger();

        // The returned result.
        $result = [
            'status' => $status,
            'warnings' => $warnings,
        ];
        return $result;
    }

    /**
     * Defines the inputs for the web service method.
     *
     * @return \core_external\external_function_parameters
     */
    public static function view_parameters() {
        return new external_function_parameters([
            'id' => new external_value(PARAM_INT, 'The instance id of a Allocation form activity', VALUE_REQUIRED),
        ]);
    }

    /**
     * Defines the output of the web service.
     *
     * @return \core_external\external_function_parameters
     */
    public static function view_returns() {
        return new external_function_parameters(
            [
                'status' => new external_value(PARAM_BOOL, 'status: true if success'),
                'warnings' => new external_warnings(),
            ]
        );
    }
}
