<?php
// This file is part of the allocation form plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace mod_allocationform\external;
use core_external\external_function_parameters;
use core_external\external_single_structure;
use core_external\external_value;
use core_external\external_warnings;
use mod_allocationform\activity;
use mod_allocationform\allocation;

/**
 * Allows a user to submit their choices to be used in an allocation.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class choices extends \core_external\external_api {
    /**
     * Submit choices for an allocation form.
     *
     * @param int $id The id of an allocation form.
     * @param array $choices
     * @return array
     */
    public static function submit($id, $choices) {
        global $DB, $USER;
        $forvalidation = [
            'id' => $id,
            'choices' => $choices,
        ];
        $params = self::validate_parameters(self::submit_parameters(), $forvalidation);
        // Check the allocation form exists.
        list($course, $cm) = get_course_and_cm_from_instance($params['id'], 'allocationform');
        $context = \context_module::instance($cm->id);
        self::validate_context($context);
        // Setup the defaults for the output.
        $status = true;
        $warnings = [];
        if (!activity::user_can_be_allocated($USER->id, $cm)) {
            // User can never be allocated.
            $status = false;
            $warnings[] = [
                'item' => 'allocationform',
                'itemid' => $params['id'],
                'warningcode' => 'nopermission',
                'message' => 'The user does not have the correct permissions to be allocated to the form',
            ];
        } else if (!activity::user_can_choose($USER->id, $cm)) {
            // The user cannot choose right now.
            $status = false;
            $warnings[] = [
                'item' => 'allocationform',
                'itemid' => $params['id'],
                'warningcode' => 'choiceincorrectstate',
                'message' => 'The form is in the wrong state for allocation',
            ];
        }
        // Validate that the correct number of choices have been made.
        $expectedchoices = (int) $cm->customdata['choices'];
        if ($cm->customdata['notwant']) {
            $expectedchoices++;
        }
        if ($expectedchoices !== count($params['choices'])) {
            $status = false;
            $warnings[] = [
                'item' => 'allocationform',
                'itemid' => $params['id'],
                'warningcode' => 'incorrectnumberofchoices',
                'message' => 'The form does not have a valid number of choices',
            ];
        }
        // Check that the choices are for valid options.
        $validchoices = activity::get_valid_choices($USER->id, $cm->instance);
        $usedchoices = []; // An array of ids selected.
        // We do not want to generate mmultiple warnings of the same type for the same option.
        $validwarned = [];
        $duplicatewarned = [];
        foreach ($params['choices'] as $key => $userchoice) {
            $choicemade = !empty($userchoice);
            if ($key == 'notwant' && !$choicemade) {
                // The user is not required to select an option they will not be allocated to.
                continue;
            }
            if (!$choicemade && !isset($validwarned[$userchoice])) {
                // The id is not a valid choice.
                $status = false;
                $warnings[] = [
                    'item' => 'allocationform_option',
                    'itemid' => $userchoice,
                    'warningcode' => 'choicerequired',
                    'message' => 'A choice must be made',
                ];
                $validwarned[$userchoice] = $userchoice;
            } else if (!isset($validchoices[$userchoice]) && !isset($validwarned[$userchoice])) {
                // The id is not a valid choice.
                $status = false;
                $warnings[] = [
                    'item' => 'allocationform_option',
                    'itemid' => $userchoice,
                    'warningcode' => 'choiceinvalid',
                    'message' => 'The choice is not valid for the form',
                ];
                $validwarned[$userchoice] = $userchoice;
            }
            if ($choicemade && isset($usedchoices[$userchoice]) && !isset($duplicatewarned[$userchoice])) {
                // The choice has been selected by the user more than one time.
                $status = false;
                $warnings[] = [
                    'item' => 'allocationform_option',
                    'itemid' => $userchoice,
                    'warningcode' => 'choicealreadyselected',
                    'message' => 'The choice has been selected multiple times',
                ];
                $duplicatewarned[$userchoice] = $userchoice;
            }
            $usedchoices[$userchoice] = $userchoice;
        }
        // Record the choices.
        if ($status === true) {
            // Nothing obvious wrong so try to save.
            $processor = new allocation($USER->id, $cm->instance);
            $status = $processor->process_webservice($params['choices'], $course);
            if ($status === false) {
                // The save falied.
                $warnings[] = [
                    'item' => 'allocationform',
                    'itemid' => $params['id'],
                    'warningcode' => 'savefail',
                    'message' => 'The choices could not be saved',
                ];
            }
        }
        // Send a response.
        $result = [
            'status' => $status,
            'warnings' => $warnings,
        ];
        return $result;
    }

    /**
     * Defines the inputs for the web service method.
     *
     * @return \core_external\external_function_parameters
     */
    public static function submit_parameters() {
        return new external_function_parameters([
            'id' => new external_value(PARAM_INT, 'The instance id of a Allocation form activity', VALUE_REQUIRED),
            'choices' => new external_single_structure([
                'choice1' => new external_value(PARAM_INT, 'The id of the option', VALUE_REQUIRED),
                'choice2' => new external_value(PARAM_INT, 'The id of the option', VALUE_OPTIONAL),
                'choice3' => new external_value(PARAM_INT, 'The id of the option', VALUE_OPTIONAL),
                'choice4' => new external_value(PARAM_INT, 'The id of the option', VALUE_OPTIONAL),
                'choice5' => new external_value(PARAM_INT, 'The id of the option', VALUE_OPTIONAL),
                'choice6' => new external_value(PARAM_INT, 'The id of the option', VALUE_OPTIONAL),
                'choice7' => new external_value(PARAM_INT, 'The id of the option', VALUE_OPTIONAL),
                'choice8' => new external_value(PARAM_INT, 'The id of the option', VALUE_OPTIONAL),
                'choice9' => new external_value(PARAM_INT, 'The id of the option', VALUE_OPTIONAL),
                'choice10' => new external_value(PARAM_INT, 'The id of the option', VALUE_OPTIONAL),
                'notwant' => new external_value(PARAM_INT, 'The id of a an option that is not wanted', VALUE_OPTIONAL),
            ], 'The choices the user has made'),
        ]);
    }

    /**
     * Defines the output of the web service.
     *
     * @return \core_external\external_function_parameters
     */
    public static function submit_returns() {
        return new external_function_parameters(
            [
                'status' => new external_value(PARAM_BOOL, 'status: true if success'),
                'warnings' => new external_warnings(),
            ]
        );
    }
}
