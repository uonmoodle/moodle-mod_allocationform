<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

declare(strict_types=1);

namespace mod_allocationform\reportbuilder\datasource;

use core_course\reportbuilder\local\entities\course_category;
use core_reportbuilder\datasource;
use core_reportbuilder\local\entities\course;
use core_reportbuilder\local\entities\user;
use mod_allocationform\reportbuilder\entities\allocationform;
use mod_allocationform\reportbuilder\entities\allocationform_allocations;

/**
 * Adds allocations made by users to custom reports.
 *
 * The data source allows details of the:
 *
 *  - Allocations made
 *  - users
 *  - allocation form activity
 *  - course
 *  - category
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2024 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class allocations extends datasource {
    /**
     * Gets the localised name for the data source.
     *
     * @return string
     */
    #[Override]
    public static function get_name(): string {
        return get_string('allocations', 'mod_allocationform');
    }

    /**
     * Configures the entities that can be used in by the data source.
     */
    #[Override]
    protected function initialise(): void {
        $allocations = new allocationform_allocations();
        $allocations->set_table_alias('allocationform_allocations', 'a');
        $this->add_entity($allocations);

        $form = new allocationform();
        $form->set_table_alias('allocationform', 'af');
        $form->add_join('JOIN {allocationform} af ON af.id = a.formid');
        $this->add_entity($form);

        $user = new user();
        $user->set_table_alias('user', 'u');
        $user->add_join('JOIN {user} u ON u.id = a.userid');
        $this->add_entity($user);

        $course = new course();
        $course->set_table_alias('course', 'co');
        $course->add_joins($form->get_joins());
        $course->add_join('JOIN {course} co ON co.id = af.course');
        $this->add_entity($course);

        $category = new course_category();
        $category->set_table_alias('course_categories', 'cat');
        $category->add_joins($course->get_joins());
        $category->add_join('JOIN {course_categories} cat ON cat.id = co.category');
        $this->add_entity($category);

        $this->set_main_table('allocationform_allocations', 'a');

        $this->add_all_from_entities();
    }

    /**
     * List of columns that will be included in the default report.
     *
     * @return string[]
     */
    #[Override]
    public function get_default_columns(): array {
        return [
            'allocationform:namelink',
            'user:fullname',
            'allocationform_allocations:allocation',
        ];
    }

    /**
     * Defines sorting applied to the default report.
     *
     * @return array
     */
    #[Override]
    public function get_default_column_sorting(): array {
        return [
            'allocationform:namelink' => SORT_ASC,
            'user:fullname' => SORT_ASC,
        ];
    }

    /**
     * The default filters that the end user of the report may use.
     *
     * @return string[]
     */
    #[Override]
    public function get_default_filters(): array {
        return [
            'user:fullname',
        ];
    }

    /**
     * The default conditions used to return results.
     *
     * @return string[]
     */
    #[Override]
    public function get_default_conditions(): array {
        return [
            'allocationform:name',
        ];
    }
}
