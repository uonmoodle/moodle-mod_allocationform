<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

declare(strict_types=1);

namespace mod_allocationform\reportbuilder\datasource;

use core_course\reportbuilder\local\entities\course_category;
use core_reportbuilder\datasource;
use core_reportbuilder\local\entities\course;
use core_reportbuilder\local\entities\user;
use mod_allocationform\reportbuilder\entities\allocationform;
use mod_allocationform\reportbuilder\entities\allocationform_choices;

/**
 * Adds allocation form choices to custom reports.
 *
 * The data source allows details of the:
 *
 * - choices the user made
 * - users
 * - allocation form activity
 * - course
 * - category
 *
 * to be included in the report.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2024 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class choices extends datasource {
    /**
     * The display name of the data source.
     *
     * @return string
     */
    #[Override]
    public static function get_name(): string {
        return get_string('choices', 'mod_allocationform');
    }

    /**
     * Configures the entities that can be used in by the data source.
     */
    #[Override]
    protected function initialise(): void {
        $choices = new allocationform_choices();
        $choices->set_table_alias('allocationform_choices', 'c');
        $this->add_entity($choices);

        $this->set_main_table('allocationform_choices', 'c');

        $form = new allocationform();
        $form->set_table_alias('allocationform', 'a');
        $form->add_join('JOIN {allocationform} a ON a.id = c.formid');
        $this->add_entity($form);

        $user = new user();
        $user->set_table_alias('user', 'u');
        $user->add_join('JOIN {user} u ON u.id = c.userid');
        $this->add_entity($user);

        $course = new course();
        $course->set_table_alias('course', 'co');
        $course->add_joins($form->get_joins());
        $course->add_join('JOIN {course} co ON co.id = a.course');
        $this->add_entity($course);

        $category = new course_category();
        $category->set_table_alias('course_categories', 'cat');
        $category->add_joins($course->get_joins());
        $category->add_join('JOIN {course_categories} cat ON cat.id = co.category');
        $this->add_entity($category);

        $this->add_all_from_entities();
    }

    /**
     * List of columns that will be included in the default report.
     *
     * @return string[]
     */
    #[Override]
    public function get_default_columns(): array {
        return [
            'allocationform:namelink',
            'user:fullname',
            'allocationform_choices:choice1',
            'allocationform_choices:choice2',
            'allocationform_choices:choice3',
            'allocationform_choices:choice4',
            'allocationform_choices:choice5',
            'allocationform_choices:choice6',
            'allocationform_choices:choice7',
            'allocationform_choices:choice8',
            'allocationform_choices:choice9',
            'allocationform_choices:choice10',
            'allocationform_choices:notwant',
        ];
    }

    /**
     * Defines sorting applied to the default report.
     *
     * @return array
     */
    #[Override]
    public function get_default_column_sorting(): array {
        return [
            'allocationform:namelink' => SORT_ASC,
            'user:fullname' => SORT_ASC,
        ];
    }

    /**
     * The default filters that the end user of the report may use.
     *
     * @return string[]
     */
    #[Override]
    public function get_default_filters(): array {
        return [
            'user:fullname',
        ];
    }

    /**
     * The default conditions used to return results.
     *
     * @return string[]
     */
    #[Override]
    public function get_default_conditions(): array {
        return [
            'allocationform:name',
        ];
    }
}
