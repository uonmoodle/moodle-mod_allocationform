<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

declare(strict_types=1);

namespace mod_allocationform\reportbuilder\entities;

use core\context_helper;
use core_reportbuilder\local\entities\base;
use core_reportbuilder\local\filters\date;
use core_reportbuilder\local\filters\select;
use core_reportbuilder\local\filters\text;
use core_reportbuilder\local\helpers\format;
use core_reportbuilder\local\report\column;
use core_reportbuilder\local\report\filter;
use lang_string;
use mod_allocationform\helper;
use mod_allocationform\reportbuilder\formatters\allocationform as formatter;
use stdClass;

/**
 * Describes the allocationform_options table for the report builder API.
 *
 * This table stores the options that users may be allocated to.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2024 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class allocationform extends base {
    /**
     * Defines the default aliases for tables used in the entity.
     *
     * @return string[]
     */
    #[Override]
    protected function get_default_table_aliases(): array {
        return [
            'allocationform' => 'a',
            'context' => 'ctx',
            'course_modules' => 'cm',
            'modules' => 'm',
        ];
    }

    /**
     * The display name of the entity.
     *
     * @return lang_string
     */
    #[Override]
    protected function get_default_entity_title(): lang_string {
        return new lang_string('allocationforms', 'mod_allocationform');
    }

    /**
     * Sets up the allocation form entity.
     *
     * @return base
     */
    #[Override]
    public function initialise(): base {
        $columns = $this->get_all_columns();
        foreach ($columns as $column) {
            $this->add_column($column);
        }

        $filters = $this->get_all_filters();
        foreach ($filters as $filter) {
            $this->add_filter($filter);
            $this->add_condition($filter);
        }
        return $this;
    }

    /**
     * Get all the columns for the allocation form table we wish to make available in report builder.
     *
     * @return column[]
     */
    protected function get_all_columns(): array {
        $entityname = $this->get_entity_name();
        $alias = $this->get_table_alias('allocationform');
        $contextalias = $this->get_table_alias('context');

        $name = new column(
            'name',
            new lang_string('name'),
            $entityname
        );
        $name->add_field("{$alias}.name");
        $name->add_joins($this->get_joins());
        $name->set_type(column::TYPE_TEXT);
        $name->set_is_sortable(true);

        $namelink = new column(
            'namelink',
            new lang_string('allocationforms:namelink', 'mod_allocationform'),
            $entityname
        );
        $namelink->add_field("{$alias}.name");
        $namelink->add_field("{$alias}.id");
        $namelink->add_joins($this->get_joins());
        $namelink->set_type(column::TYPE_TEXT);
        $namelink->set_is_sortable(true);
        $namelink->add_callback([formatter::class, 'name']);

        $intro = new column(
            'intro',
            new lang_string('moduleintro'),
            $entityname
        );
        $intro->add_field("{$alias}.intro");
        $intro->add_field("{$alias}.introformat");
        $intro->add_fields(context_helper::get_preload_record_columns_sql($contextalias));
        $intro->add_joins($this->get_joins());
        $intro->add_join($this->get_context_join());
        $intro->set_type(column::TYPE_LONGTEXT);
        $intro->add_callback([formatter::class, 'intro']);

        $options = new column(
            'options',
            new lang_string('allocationforms:options', 'mod_allocationform'),
            $entityname
        );
        $options->add_field("{$alias}.numberofchoices");
        $options->add_field("{$alias}.notwant");
        $options->add_field("{$alias}.numberofallocations");
        $options->add_joins($this->get_joins());
        $options->set_type(column::TYPE_TEXT);
        $options->set_callback([formatter::class, 'options']);

        $startdate = new column(
            'startdate',
            new lang_string('startdate', 'mod_allocationform'),
            $entityname
        );
        $startdate->add_field("{$alias}.startdate");
        $startdate->add_joins($this->get_joins());
        $startdate->set_type(column::TYPE_TIMESTAMP);
        $startdate->add_callback([format::class, 'userdate']);
        $startdate->set_is_sortable(true);

        $deadline = new column(
            'deadline',
            new lang_string('deadline', 'mod_allocationform'),
            $entityname
        );
        $deadline->add_field("{$alias}.deadline");
        $deadline->add_joins($this->get_joins());
        $deadline->set_type(column::TYPE_TIMESTAMP);
        $deadline->add_callback([format::class, 'userdate']);
        $deadline->set_is_sortable(true);

        $processed = new column(
            'processed',
            new lang_string('allocationforms:processed', 'mod_allocationform'),
            $entityname
        );
        $processed->add_field("{$alias}.processed");
        $processed->add_joins($this->get_joins());
        $processed->set_type(column::TYPE_TIMESTAMP);
        $processed->add_callback([format::class, 'userdate']);
        $processed->set_is_sortable(true);

        $state = new column(
            'state',
            new lang_string('allocationforms:state', 'mod_allocationform'),
            $entityname
        );
        $state->add_field("{$alias}.state");
        $state->add_joins($this->get_joins());
        $state->set_type(column::TYPE_TEXT);
        $state->set_callback([formatter::class, 'state']);
        $state->set_is_sortable(true);

        return [
            $name,
            $namelink,
            $intro,
            $options,
            $startdate,
            $deadline,
            $processed,
            $state,
        ];
    }

    /**
     * Define the items on the table that we can filter on.
     *
     * @return array
     */
    protected function get_all_filters(): array {
        $alias = $this->get_table_alias('allocationform');

        $name = new filter(
            text::class,
            'name',
            new lang_string('name'),
            $this->get_entity_name(),
            "{$alias}.name"
        );
        $name->add_joins($this->get_joins());

        $startdate = new filter(
            date::class,
            'startdate',
            new lang_string('startdate', 'mod_allocationform'),
            $this->get_entity_name(),
            "{$alias}.startdate"
        );
        $startdate->add_joins($this->get_joins());

        $deadline = new filter(
            date::class,
            'deadline',
            new lang_string('deadline', 'mod_allocationform'),
            $this->get_entity_name(),
            "{$alias}.deadline"
        );
        $deadline->add_joins($this->get_joins());

        $processed = new filter(
            date::class,
            'processed',
            new lang_string('allocationforms:processed', 'mod_allocationform'),
            $this->get_entity_name(),
            "{$alias}.processed"
        );
        $processed->add_joins($this->get_joins());

        $state = new filter(
            select::class,
            'state',
            new lang_string('allocationforms:state', 'mod_allocationform'),
            $this->get_entity_name(),
            "{$alias}.state"
        );
        $state->set_options([
            helper::STATE_EDITING => new lang_string('editingmode', 'mod_allocationform'),
            helper::STATE_READY => new lang_string('readymode', 'mod_allocationform'),
            helper::STATE_PROCESS => new lang_string('processmode', 'mod_allocationform'),
            helper::STATE_REVIEW => new lang_string('reviewmode', 'mod_allocationform'),
            helper::STATE_PROCESSED => new lang_string('processedmode', 'mod_allocationform'),
        ]);
        $state->add_joins($this->get_joins());

        return [
            $name,
            $startdate,
            $deadline,
            $processed,
            $state,
        ];
    }

    /**
     * Return sql fragment for joining on the context table
     *
     * @return string
     */
    protected function get_context_join(): string {
        $alias = $this->get_table_alias('allocationform');
        $contextalias = $this->get_table_alias('context');
        $cmalias = $this->get_table_alias('course_modules');
        $modulealias = $this->get_table_alias('modules');

        $level = CONTEXT_MODULE;

        return "JOIN {course_modules} {$cmalias} ON {$cmalias}.instance = {$alias}.id " .
               "JOIN {modules} {$modulealias} ON {$modulealias}.id = {$cmalias}.module ".
                "AND {$modulealias}.name = 'allocationform' " .
               "JOIN {context} {$contextalias} ON {$contextalias}.instanceid = {$cmalias}.id " .
                "AND {$contextalias}.contextlevel = {$level}";
    }
}
