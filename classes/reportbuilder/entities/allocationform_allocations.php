<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

declare(strict_types=1);

namespace mod_allocationform\reportbuilder\entities;

use core_reportbuilder\local\entities\base;
use core_reportbuilder\local\filters\text;
use core_reportbuilder\local\report\column;
use core_reportbuilder\local\report\filter;
use lang_string;

/**
 * Describes the allocationform_allocations table for the report builder API.
 *
 * This table stores the allocations that have been made in forms.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2024 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class allocationform_allocations extends base {
    /**
     * Defines the default aliases for tables used in the entity.
     *
     * @return string[]
     */
    #[Override]
    protected function get_default_table_aliases(): array {
        return [
            'allocationform_allocations' => 'a',
            'allocationform_options' => 'a0',
        ];
    }

    /**
     * The display name of the entity.
     *
     * @return lang_string
     */
    #[Override]
    protected function get_default_entity_title(): lang_string {
        return new lang_string("allocationform_allocations", 'mod_allocationform');
    }

    /**
     * Sets up the allocation form entity.
     *
     * @return base
     */
    #[Override]
    public function initialise(): base {
        $columns = $this->get_all_columns();
        foreach ($columns as $column) {
            $this->add_column($column);
        }

        $filters = $this->get_all_filters();
        foreach ($filters as $filter) {
            $this->add_filter($filter);
            $this->add_condition($filter);
        }
        return $this;
    }

    /**
     * Get all the columns for the allocation table we wish to make available in report builder.
     *
     * @return column[]
     */
    protected function get_all_columns(): array {
        $entityname = $this->get_entity_name();

        $alias = $this->get_table_alias('allocationform_allocations');
        $optionsalias = $this->get_table_alias('allocationform_options');

        $allocation = new column(
            'allocation',
            new lang_string('allocationform_allocations:allocation', 'mod_allocationform'),
            $entityname
        );
        $allocation->add_joins($this->get_joins());
        $allocation->add_join("JOIN {allocationform_options} {$optionsalias} ON {$optionsalias}.id = {$alias}.allocation");
        $allocation->add_field("{$optionsalias}.name");
        $allocation->set_type(column::TYPE_TEXT);
        $allocation->set_is_sortable(true);

        return [
            $allocation,
        ];
    }

    /**
     * Define the things  that we can filter on.
     *
     * @return array
     */
    protected function get_all_filters(): array {
        $alias = $this->get_table_alias('allocationform_allocations');
        $optionsalias = $this->get_table_alias('allocationform_options');

        $allocation = new filter(
            text::class,
            'allocation',
            new lang_string('allocationform_allocations:allocation', 'mod_allocationform'),
            $this->get_entity_name(),
            "{$optionsalias}.name"
        );
        $allocation->add_joins($this->get_joins());
        $allocation->add_join("JOIN {allocationform_options} {$optionsalias} ON {$optionsalias}.id = {$alias}.allocation");

        return [
            $allocation,
        ];
    }
}
