<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

declare(strict_types=1);

namespace mod_allocationform\reportbuilder\entities;

use core_reportbuilder\local\entities\base;
use core_reportbuilder\local\filters\text;
use core_reportbuilder\local\report\column;
use core_reportbuilder\local\report\filter;
use lang_string;

/**
 * Describes the allocationform_choicess table for the report builder API.
 *
 * This table stores the choices that a user has made.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2024 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class allocationform_choices extends base {
    /**
     * Defines the default aliases for tables used in the entity.
     *
     * @return string[]
     */
    #[Override]
    protected function get_default_table_aliases(): array {
        return [
            'allocationform_choices' => 'c',
        ];
    }

    /**
     * The display name of the entity.
     *
     * @return lang_string
     */
    #[Override]
    protected function get_default_entity_title(): lang_string {
        return new lang_string("allocationform_choices", 'mod_allocationform');
    }

    /**
     * Sets up the allocation form entity.
     *
     * @return base
     */
    #[Override]
    public function initialise(): base {
        $columns = $this->get_all_columns();
        foreach ($columns as $column) {
            $this->add_column($column);
        }

        $filters = $this->get_all_filters();
        foreach ($filters as $filter) {
            $this->add_filter($filter);
            $this->add_condition($filter);
        }
        return $this;
    }

    /**
     * Gets all the columns of data that we want user to be able to view.
     *
     * When users will always have a first choice, all the other options are
     * optional depending on the activity settings.
     *
     * @return column[]
     */
    protected function get_all_columns(): array {
        $entityname = $this->get_entity_name();
        $alias = $this->get_table_alias('allocationform_choices');
        $choice1 = new column(
            'choice1',
            new lang_string('choice', 'mod_allocationform', ['choice' => '1']),
            $entityname
        );
        $choice1->add_joins($this->get_joins());
        $choice1->add_join("JOIN {allocationform_options} choice1 ON choice1.id = {$alias}.choice1");
        $choice1->add_field("choice1.name");
        $choice1->set_type(column::TYPE_TEXT);
        $choice1->set_is_sortable(true);

        $choice2 = new column(
            'choice2',
            new lang_string('choice', 'mod_allocationform', ['choice' => '2']),
            $entityname
        );
        $choice2->add_joins($this->get_joins());
        $choice2->add_join("LEFT JOIN {allocationform_options} choice2 ON choice2.id = {$alias}.choice2");
        $choice2->add_field("choice2.name");
        $choice2->set_type(column::TYPE_TEXT);
        $choice2->set_is_sortable(true);

        $choice3 = new column(
            'choice3',
            new lang_string('choice', 'mod_allocationform', ['choice' => '3']),
            $entityname
        );
        $choice3->add_joins($this->get_joins());
        $choice3->add_join("LEFT JOIN {allocationform_options} choice3 ON choice3.id = {$alias}.choice3");
        $choice3->add_field("choice3.name");
        $choice3->set_type(column::TYPE_TEXT);
        $choice3->set_is_sortable(true);

        $choice4 = new column(
            'choice4',
            new lang_string('choice', 'mod_allocationform', ['choice' => '4']),
            $entityname
        );
        $choice4->add_joins($this->get_joins());
        $choice4->add_join("LEFT JOIN {allocationform_options} choice4 ON choice4.id = {$alias}.choice4");
        $choice4->add_field("choice4.name");
        $choice4->set_type(column::TYPE_TEXT);
        $choice4->set_is_sortable(true);

        $choice5 = new column(
            'choice5',
            new lang_string('choice', 'mod_allocationform', ['choice' => '5']),
            $entityname
        );
        $choice5->add_joins($this->get_joins());
        $choice5->add_join("LEFT JOIN {allocationform_options} choice5 ON choice5.id = {$alias}.choice5");
        $choice5->add_field("choice5.name");
        $choice5->set_type(column::TYPE_TEXT);
        $choice5->set_is_sortable(true);

        $choice6 = new column(
            'choice6',
            new lang_string('choice', 'mod_allocationform', ['choice' => '6']),
            $entityname
        );
        $choice6->add_joins($this->get_joins());
        $choice6->add_join("LEFT JOIN {allocationform_options} choice6 ON choice6.id = {$alias}.choice6");
        $choice6->add_field("choice6.name");
        $choice6->set_type(column::TYPE_TEXT);
        $choice6->set_is_sortable(true);

        $choice7 = new column(
            'choice7',
            new lang_string('choice', 'mod_allocationform', ['choice' => '7']),
            $entityname
        );
        $choice7->add_joins($this->get_joins());
        $choice7->add_join("LEFT JOIN {allocationform_options} choice7 ON choice7.id = {$alias}.choice7");
        $choice7->add_field("choice7.name");
        $choice7->set_type(column::TYPE_TEXT);
        $choice7->set_is_sortable(true);

        $choice8 = new column(
            'choice8',
            new lang_string('choice', 'mod_allocationform', ['choice' => '8']),
            $entityname
        );
        $choice8->add_joins($this->get_joins());
        $choice8->add_join("LEFT JOIN {allocationform_options} choice8 ON choice8.id = {$alias}.choice8");
        $choice8->add_field("choice8.name");
        $choice8->set_type(column::TYPE_TEXT);
        $choice8->set_is_sortable(true);

        $choice9 = new column(
            'choice9',
            new lang_string('choice', 'mod_allocationform', ['choice' => '9']),
            $entityname
        );
        $choice9->add_joins($this->get_joins());
        $choice9->add_join("LEFT JOIN {allocationform_options} choice9 ON choice9.id = {$alias}.choice9");
        $choice9->add_field("choice9.name");
        $choice9->set_type(column::TYPE_TEXT);
        $choice9->set_is_sortable(true);

        $choice10 = new column(
            'choice10',
            new lang_string('choice', 'mod_allocationform', ['choice' => '10']),
            $entityname
        );
        $choice10->add_joins($this->get_joins());
        $choice10->add_join("LEFT JOIN {allocationform_options} choice10 ON choice10.id = {$alias}.choice10");
        $choice10->add_field("choice10.name");
        $choice10->set_type(column::TYPE_TEXT);
        $choice10->set_is_sortable(true);

        $notwant = new column(
            'notwant',
            new lang_string('allocationform_choices:notwant', 'mod_allocationform'),
            $entityname
        );
        $notwant->add_joins($this->get_joins());
        $notwant->add_join("LEFT JOIN {allocationform_options} notwant ON notwant.id = {$alias}.notwant");
        $notwant->add_field("notwant.name");
        $notwant->set_type(column::TYPE_TEXT);
        $notwant->set_is_sortable(true);

        return [
            $choice1,
            $choice2,
            $choice3,
            $choice4,
            $choice5,
            $choice6,
            $choice7,
            $choice8,
            $choice9,
            $choice10,
            $notwant,
        ];
    }

    /**
     * Define the things  that we can filter on.
     *
     * @return array
     */
    protected function get_all_filters(): array {
        $alias = $this->get_table_alias('allocationform_choices');

        $choice1 = new filter(
            text::class,
            'choice1',
            new lang_string('choice', 'mod_allocationform', ['choice' => '1']),
            $this->get_entity_name(),
            "choice1.name"
        );
        $choice1->add_joins($this->get_joins());
        $choice1->add_join("JOIN {allocationform_options} choice1 ON choice1.id = {$alias}.choice1");

        $choice2 = new filter(
            text::class,
            'choice2',
            new lang_string('choice', 'mod_allocationform', ['choice' => '2']),
            $this->get_entity_name(),
            "choice2.name"
        );
        $choice2->add_joins($this->get_joins());
        $choice2->add_join("LEFT JOIN {allocationform_options} choice2 ON choice2.id = {$alias}.choice2");

        $choice3 = new filter(
            text::class,
            'choice3',
            new lang_string('choice', 'mod_allocationform', ['choice' => '3']),
            $this->get_entity_name(),
            "choice3.name"
        );
        $choice3->add_joins($this->get_joins());
        $choice3->add_join("LEFT JOIN {allocationform_options} choice3 ON choice3.id = {$alias}.choice3");

        $choice4 = new filter(
            text::class,
            'choice4',
            new lang_string('choice', 'mod_allocationform', ['choice' => '4']),
            $this->get_entity_name(),
            "choice4.name"
        );
        $choice4->add_joins($this->get_joins());
        $choice4->add_join("LEFT JOIN {allocationform_options} choice4 ON choice4.id = {$alias}.choice4");

        $choice5 = new filter(
            text::class,
            'choice5',
            new lang_string('choice', 'mod_allocationform', ['choice' => '5']),
            $this->get_entity_name(),
            "choice5.name"
        );
        $choice5->add_joins($this->get_joins());
        $choice5->add_join("LEFT JOIN {allocationform_options} choice5 ON choice5.id = {$alias}.choice5");

        $choice6 = new filter(
            text::class,
            'choice6',
            new lang_string('choice', 'mod_allocationform', ['choice' => '6']),
            $this->get_entity_name(),
            "choice6.name"
        );
        $choice6->add_joins($this->get_joins());
        $choice6->add_join("LEFT JOIN {allocationform_options} choice6 ON choice6.id = {$alias}.choice6");

        $choice7 = new filter(
            text::class,
            'choice7',
            new lang_string('choice', 'mod_allocationform', ['choice' => '7']),
            $this->get_entity_name(),
            "choice7.name"
        );
        $choice7->add_joins($this->get_joins());
        $choice7->add_join("LEFT JOIN {allocationform_options} choice7 ON choice7.id = {$alias}.choice7");

        $choice8 = new filter(
            text::class,
            'choice8',
            new lang_string('choice', 'mod_allocationform', ['choice' => '8']),
            $this->get_entity_name(),
            "choice8.name"
        );
        $choice8->add_joins($this->get_joins());
        $choice8->add_join("LEFT JOIN {allocationform_options} choice8 ON choice8.id = {$alias}.choice8");

        $choice9 = new filter(
            text::class,
            'choice9',
            new lang_string('choice', 'mod_allocationform', ['choice' => '9']),
            $this->get_entity_name(),
            "choice9.name"
        );
        $choice9->add_joins($this->get_joins());
        $choice9->add_join("LEFT JOIN {allocationform_options} choice9 ON choice9.id = {$alias}.choice9");

        $choice10 = new filter(
            text::class,
            'choice10',
            new lang_string('choice', 'mod_allocationform', ['choice' => '10']),
            $this->get_entity_name(),
            "choice10.name"
        );
        $choice10->add_joins($this->get_joins());
        $choice10->add_join("LEFT JOIN {allocationform_options} choice10 ON choice10.id = {$alias}.choice10");

        $notwant = new filter(
            text::class,
            'notwant',
            new lang_string('allocationform_choices:notwant', 'mod_allocationform'),
            $this->get_entity_name(),
            "notwant.name"
        );
        $notwant->add_joins($this->get_joins());
        $notwant->add_join("LEFT JOIN {allocationform_options} notwant ON notwant.id = {$alias}.notwant");

        return [
            $choice1,
            $choice2,
            $choice3,
            $choice4,
            $choice5,
            $choice6,
            $choice7,
            $choice8,
            $choice9,
            $choice10,
            $notwant,
        ];
    }
}
