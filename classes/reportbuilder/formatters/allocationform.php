<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

declare(strict_types=1);

namespace mod_allocationform\reportbuilder\formatters;

use core\context;
use core\context_helper;
use mod_allocationform\helper;
use stdClass;

/**
 * Formatters used by entity columns in this plugin.
 *
 * @package    mod_allocationform
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2024 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class allocationform {
    /**
     * Callback used to format the introduction text of an activity.
     *
     * It requires that the following fields are included
     * - intro (this field must be first)
     * - introformat
     * - All the context fields defined by {@see context_helper::get_preload_record_columns_sql}
     *
     * @param string|null $content The value to be formatted
     * @param stdClass $intro The for the intro field
     * @return string The text we want to display
     */
    public static function intro(?string $content, stdClass $intro): string {
        if ($content === null) {
            return '';
        }

        $contextid = $intro->ctxid;

        context_helper::preload_from_record($intro);
        $context = context::instance_by_id($contextid);

        return format_text($content, $intro->introformat, ['context' => $context]);
    }

    /**
     * Adds a link to the name of an allocation form.
     *
     * @param string $name
     * @param stdClass $data
     * @return string
     */
    public static function name(string $name, stdClass $data): string {
        $url = new \moodle_url('/mod/allocationform/view.php', ['id' => $data->id]);
        return \html_writer::link($url, $name);
    }

    /**
     * Call back for formatting the allocation form options.
     *
     * Requires database fields named to match the string placeholders.
     *
     * @param mixed $notused We do not use this, but it is required by the callback signature.
     * @param stdClass $options
     * @return string
     */
    public static function options($notused, stdClass $options): string {
        if ($options->notwant) {
            $options->notwant = get_string('yes');
        } else {
            $options->notwant = get_string('no');
        }

        return get_string('allocationforms:options_text', 'mod_allocationform', $options);
    }

    /**
     * Callback to format the state of the form into an end user value.
     *
     * @param int|string $state
     * @return string
     */
    public static function state(int|string $state): string {
        switch ($state) {
            case helper::STATE_EDITING:
                return get_string('editingmode', 'mod_allocationform');
            case helper::STATE_READY:
                return get_string('readymode', 'mod_allocationform');
            case helper::STATE_PROCESS:
                return get_string('processmode', 'mod_allocationform');
            case helper::STATE_REVIEW:
                return get_string('reviewmode', 'mod_allocationform');
            case helper::STATE_PROCESSED:
                return get_string('processedmode', 'mod_allocationform');
        }

        // It is in an unknown state.
        return '';
    }
}
