@uon @mod @mod_allocationform
Feature: Backup and restore
  In order to backup, duplicate, import and restore  an allocation form
  As a teacher
  I need the backup and restore functionality to work

  @javascript
  Scenario: Test back up and restore by duplicating the activity
    Given the following "users" exist:
      | username | firstname | lastname | email                |
      | teacher1 | Darrell   | Teacher1 | teacher1@example.com |
    And the following "courses" exist:
      | fullname | shortname | enablecompletion | showcompletionconditions |
      | Course 1 | C1        | 1                | 1                        |
    And the following "course enrolments" exist:
      | user | course | role           |
      | teacher1 | C1 | editingteacher |
      | teacher1 | C1 | student        |
    And the following "activity" exists:
      | activity        | allocationform |
      | course          | C1             |
      | idnumber        | mh1            |
      | name            | Fruit          |
      | section         | 1              |
      | numberofchoices | 2              |
      | state           | 2              |
      | deadline        | ##tomorrow##   |
    And the following allocation form options exist:
      | form | name    | allocations |
      | mh1  | Apples  | 2           |
      | mh1  | Oranges | 2           |
      | mh1  | Pears   | 2           |
    And I am on the "C1" "course" page logged in as "teacher1"
    And I turn editing mode on
    When I duplicate "Fruit" activity
    And I am on the "Fruit (copy)" "mod_allocationform > View" page
    Then I should see "Apples"
    And I should see "Oranges"
    And I should see "Pears"
    # The form should be created in the editing state, events will only be displayed when the form is active.
    And I click on "Make the form active" "link_or_button"
    And I press "Yes"
    # Test that the events for the activity were created.
    And I am on homepage
    And "Fruit (copy)" "text" in the "Timeline" "block" should be visible
