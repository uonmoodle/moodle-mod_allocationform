@mod @uon @mod_allocationform
Feature: Making choices on an allocation form
  In order to be allocated to the options I desire
  As a student
  I should be able to make choices

  Background:
    Given the following "users" exist:
      | username | firstname | lastname | email                |
      | student1 | Sam       | Student  | student1@example.com |
      | teacher1 | Bob       | Teacher  | teacher1@example.com |
    And the following "courses" exist:
      | fullname | shortname | category | summary       |
      | C1       | C1        | 0        | Test course 1 |
    And the following "course enrolments" exist:
      | user     | course | role           |
      | student1 | C1     | student        |
      | teacher1 | C1     | editingteacher |
    And the following "activities" exist:
      | activity | course | idnumber | name | intro | numberofchoices | state |
      | allocationform | C1 | A1 | Test Allocation form name | Test Allocation form description | 3 | 2 |
    And the following allocation form options exist:
      | form | name    | allocations |
      | A1   | Apples  | 2           |
      | A1   | Oranges | 2           |
      | A1   | Pears   | 2           |
      | A1   | Bananas | 2           |
      | A1   | Thorns  | 2           |

  Scenario: I should have my choices saved
    Given I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "student1"
    And allocation form is available
    When I set the following fields to these values:
      | id_choice1 | Apples  |
      | id_choice2 | Oranges |
      | id_choice3 | Bananas |
    And I press "Save changes"
    And I should see "Your choices have been saved"
    And I am on the "Test Allocation form name" "mod_allocationform > View" page
    Then the following fields match these values:
      | id_choice1 | Apples  |
      | id_choice2 | Oranges |
      | id_choice3 | Bananas |
    And the "id_choice1" select box should contain "Apples"
    And the "id_choice1" select box should contain "Oranges"
    And the "id_choice1" select box should contain "Pears"
    And the "id_choice1" select box should contain "Bananas"
    And the "id_choice1" select box should contain "Thorns"

  @app @javascript
  Scenario: I should have my choices saved in the app
    Given I entered the course "C1" as "student1" in the app
    And I press "Test Allocation form name" in the app
    When I press "Choice 1" in the app
    And I press "Apples" in the app
    And I press "OK" in the app
    And I press "Choice 2" in the app
    And I press "Oranges" in the app
    And I press "OK" in the app
    And I press "Choice 3" in the app
    And I press "Bananas" in the app
    And I press "OK" in the app
    And I press "Save" in the app
    And I pull to refresh in the app
    # Ideally we would not have to open the select to check what was selected
    # but I could not find a better way of doing this at the time.
    Then I press "Choice 1" in the app
    And "Apples" should be selected in the app
    And I press "Cancel" in the app
    And I press "Choice 2" in the app
    And "Oranges" should be selected in the app
    And I press "Cancel" in the app
    And I press "Choice 3" in the app
    And "Bananas" should be selected in the app
    And I press "Cancel" in the app

  Scenario: I should not see options I am restricted from
    Given the following allocation form restrictions exist:
      | form | option | user     |
      | A1   | Apples | student1 |
    And I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "student1"
    Then the "id_choice1" select box should contain "Oranges"
    And the "id_choice1" select box should contain "Pears"
    And the "id_choice1" select box should contain "Bananas"
    And the "id_choice1" select box should contain "Thorns"
    But the "id_choice1" select box should not contain "Apples"

  @app @javascript
  Scenario: I should not see options I am restricted from in the app
    Given the following allocation form restrictions exist:
      | form | option | user     |
      | A1   | Apples | student1 |
    And I entered the course "C1" as "student1" in the app
    And I press "Test Allocation form name" in the app
    When I press "Choice 1" in the app
    Then I should find "Oranges" in the app
    And I should find "Pears" in the app
    And I should find "Bananas" in the app
    And I should find "Thorns" in the app
    But I should not find "Apples" in the app

  Scenario: A teacher should not have choices saved
    Given I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "teacher1"
    When I set the following fields to these values:
      | id_choice1 | Apples  |
      | id_choice2 | Oranges |
      | id_choice3 | Bananas |
    And I press "Save changes"
    And I should not see "Your choices have been saved"
    And I am on the "Test Allocation form name" "mod_allocationform > View" page
    Then the following fields match these values:
      | id_choice1 |  |
      | id_choice2 |  |
      | id_choice3 |  |

  Scenario: A teacher should be able to download user choices
    Given I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "teacher1"
    Then I should see generate csv

  Scenario: A teacher should be able to switch back to editing mode
    Given I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "teacher1"
    When I switch to edit mode
    Then I should see allocation form option "Apples"
    And I should see allocation form option "Oranges"
    And I should see allocation form option "Pears"
    And I should see allocation form option "Bananas"
    And I should see allocation form option "Thorns"
