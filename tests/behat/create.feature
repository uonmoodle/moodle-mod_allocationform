@mod @uon @mod_allocationform
Feature: Creating an allocation form
  In order to use the activity
  As a teacher
  I should be able to create an instance

  @javascript
  Scenario: Create allocation form
    Given the following "users" exist:
      | username | firstname | lastname | email                |
      | teacher1 | Bob       | Teacher  | teacher1@example.com |
    And the following "courses" exist:
      | fullname | shortname | category | summary       |
      | C1       | C1        | 0        | Test course 1 |
    And the following "course enrolments" exist:
      | user     | course | role           |
      | teacher1 | C1     | editingteacher |
    And I am on the "C1" "course" page logged in as "teacher1"
    And I turn editing mode on
    When I add a "Allocation form" to section "1" and I fill the form with:
      | Allocation form name | My allocation form         |
      | Description          | This is an allocation form |
      | Number of choices    | 2                          |
      | Allocations per user | 1                          |
      | Role to be allocated | Student                    |
    And "My allocation form" "activity" should be visible
    And I am on the "My allocation form" "mod_allocationform > View" page
    Then I should see "Option list"
    And I should see "Make the form active"
