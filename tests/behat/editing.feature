@mod @uon @mod_allocationform
Feature: Editing an allocation form
  In order to setup the form
  As an editing teacher
  I should be able to edit it

  Background:
    Given the following "users" exist:
      | username | firstname | lastname | email                |
      | student1 | Sam       | Student  | student1@example.com |
      | student2 | Sid       | Student  | student1@example.com |
      | student3 | Seth      | Student  | student1@example.com |
      | teacher1 | Bob       | Teacher  | teacher1@example.com |
    And the following "courses" exist:
      | fullname | shortname | category | summary       |
      | C1       | C1        | 0        | Test course 1 |
    And the following "course enrolments" exist:
      | user     | course | role           |
      | student1 | C1     | student        |
      | student2 | C1     | student        |
      | student3 | C1     | student        |
      | teacher1 | C1     | editingteacher |
    And the following "activities" exist:
      | activity | course | idnumber | name | intro | numberofchoices |
      | allocationform | C1 | A1 | Test Allocation form name | Test Allocation form description | 2 |

  Scenario: I should be able to create options
    Given I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "teacher1"
    When I follow "Add new option"
    And I set the following fields to these values:
      | Option name         | Apples |
      | Maximum allocations | 2      |
    And I press "Save changes"
    And I follow "Add new option"
    And I set the following fields to these values:
      | Option name         | Oranges |
      | Maximum allocations | 1       |
    And I press "Save changes"
    Then I should see allocation form option "Apple"
    And I should see allocation form option "Oranges"

  Scenario: I should not be able to add options with a duplicate name
    Given the following allocation form options exist:
      | form | name   | allocations |
      | A1   | Apples | 2           |
    And I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "teacher1"
    When I follow "Add new option"
    And I set the following fields to these values:
      | Option name         | Apples |
      | Maximum allocations | 1      |
    And I press "Save changes"
    Then I should see duplicate option notice

  Scenario: I should be able to edit options
    Given the following allocation form options exist:
      | form | name    | allocations |
      | A1   | Apples  | 2           |
      | A1   | Oranges | 3           |
    And I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "teacher1"
    When I edit allocation form option "Apples"
    And I set the following fields to these values:
      | Option name         | Pears |
      | Maximum allocations | 3     |
    And I press "Save changes"
    Then I should see allocation form option "Pears"
    And I should see allocation form option "Oranges"
    But I should not see allocation form option "Apple"

  Scenario: I should be able to delete options
    Given the following allocation form options exist:
      | form | name    | allocations |
      | A1   | Apples  | 2           |
      | A1   | Oranges | 3           |
    And I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "teacher1"
    When I delete allocation form option "Apples"
    Then I should see allocation form option "Oranges"
    But I should not see allocation form option "Apple"

  Scenario: I should be able to restrict users from selecting options
    Given the following allocation form options exist:
      | form | name    | allocations |
      | A1   | Apples  | 2           |
      | A1   | Oranges | 3           |
    And I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "teacher1"
    And allocation form option "Oranges" should have "0" restricted users
    When I restrict allocation form option "Oranges" from:
      | student  | status |
      | student1 | 1      |
      | student2 | 0      |
      | student3 | 0      |
    Then allocation form option "Oranges" should have "1" restricted users
    And allocation form option "Apples" should have "0" restricted users
    And I see allocation form option "Oranges" has the following user restrictions:
      | student  | status |
      | student1 | 1      |
      | student2 | 0      |
      | student3 | 0      |
    And I see allocation form option "Apples" has the following user restrictions:
      | student  | status |
      | student1 | 0      |
      | student2 | 0      |
      | student3 | 0      |

  Scenario: Restrictions should not be affected by another form
    Given the following "activities" exist:
      | activity       | course | idnumber | name                    | intro                      |
      | allocationform | C1     | A2       | Another allocation form | Yet another wonderful form |
    And the following allocation form options exist:
      | form | name    | allocations |
      | A1   | Apples  | 2           |
      | A1   | Oranges | 3           |
      | A2   | Apples  | 2           |
      | A2   | Oranges | 3           |
    And the following allocation form restrictions exist:
      | form | option | user     |
      | A2   | Apples | student1 |
    And I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "teacher1"
    And allocation form option "Apples" should have "0" restricted users
    And I see allocation form option "Apples" has the following user restrictions:
      | student  | status |
      | student1 | 0      |
      | student2 | 0      |
      | student3 | 0      |
    When I restrict allocation form option "Orange" from:
      | student  | status |
      | student1 | 1      |
      | student2 | 0      |
      | student3 | 0      |
    And allocation form option "Oranges" should have "1" restricted users
    And I am on the "Another allocation form" "mod_allocationform > View" page
    And allocation form option "Apples" should have "1" restricted users
    And allocation form option "Oranges" should have "0" restricted users

  Scenario: When restrictions will stop a user being allocated to enough options a warning should be displayed
    Given the following allocation form options exist:
      | form | name    | allocations |
      | A1   | Apples  | 2           |
      | A1   | Oranges | 3           |
    And the following allocation form restrictions exist:
      | form | option | user     |
      | A1   | Apples | student1 |
    When I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "teacher1"
    Then workable restrictions were exceeded for 1 user(s) and 2 options required per user

  Scenario: I should be able to make the form active
    Given the following allocation form options exist:
      | form | name    | allocations |
      | A1   | Apples  | 2           |
      | A1   | Oranges | 3           |
    When I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "teacher1"
    And I make the allocation form active
    Then the following fields match these values:
      | id_choice1 |  |
      | id_choice2 |  |

  Scenario: Students should not be able to access a form in editing more
    Given I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "student1"
    Then allocation form is not available
