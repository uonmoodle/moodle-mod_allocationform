@mod @uon @mod_allocationform
Feature: Allocation form in processing mode
  In order to allow a form to allocation
  As a user
  I should have access when the form is processing

  Background:
    Given the following "users" exist:
      | username | firstname | lastname | email                |
      | student1 | Sam       | Student  | student1@example.com |
    And the following "courses" exist:
      | fullname | shortname | category | summary       |
      | C1       | C1        | 0        | Test course 1 |
    And the following "course enrolments" exist:
      | user     | course | role    |
      | student1 | C1     | student |

  Scenario: The form is in processing mode
    Given the following "activities" exist:
      | activity | course | idnumber | name | intro | numberofchoices | state |
      | allocationform | C1 | A1 | Test Allocation form name | Test Allocation form description | 1 | 4 |
    When I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "student1"
    Then the allocation form is waiting to be processed

  Scenario: The form automatically goes to processing mode when the deadline has passed
    Given the following "activities" exist:
      | activity | course | idnumber | name | intro | numberofchoices | state | deadline |
      | allocationform | C1 | A1 | Test Allocation form name | Test Allocation form description | 1 | 2 | ## yesterday ## |
    When I am on the "Test Allocation form name" "mod_allocationform > View" page logged in as "student1"
    Then the allocation form is waiting to be processed
