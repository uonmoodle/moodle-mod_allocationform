<?php
// This file is part of the Tutorial Booking activity.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tests the allocation form mod_allocationform_choices_submit web service.
 *
 * @package     mod_allocationform
 * @category    test
 * @copyright   2019 University of Nottingham
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\external;

use mod_allocationform\helper;
use core_external\external_api;

/**
 * Tests the allocation form mod_allocationform_choices_submit web service.
 *
 * @package     mod_allocationform
 * @category    test
 * @copyright   2019 University of Nottingham
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @runInSeparateProcess
 * @covers \mod_allocationform\external\choices
 * @group mod_allocationform
 * @group uon
 */
class choice_test extends \advanced_testcase {
    /**
     * Test that there are no error s when running the webservice.
     */
    public function test_submit() {
        global $DB, $USER;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $student = $DB->get_record('role', ['shortname' => 'student']);
        $course = self::getDataGenerator()->create_course();
        $student1 = self::getDataGenerator()->create_user();
        self::getDataGenerator()->enrol_user($student1->id, $course->id, $student->id);
        // Setup an allocation form.
        $params = [
            'course' => $course->id,
            'state' => helper::STATE_READY,
            'numberofchoices' => 1,
            'notwant' => 0,
            'startdate' => time() - DAYSECS,
            'deadline' => time() + DAYSECS,
        ];
        $allocation = $generator->create_instance($params);
        $option1 = $generator->create_option($allocation);
        $generator->create_option($allocation);

        self::setUser($student1);
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = [
            'id' => $allocation->id,
            'choices' => [
                'choice1' => $option1->id,
            ],
        ];
        $result = external_api::call_external_function('mod_allocationform_choices_submit', $args);

        $expectedresult = [
            'status' => true,
            'warnings' => [],
        ];
        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
    }

    /**
     * Tests picking an option that is restricted from the user.
     */
    public function test_pick_restricted() {
        global $DB, $USER;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $student = $DB->get_record('role', ['shortname' => 'student']);
        $course = self::getDataGenerator()->create_course();
        $student1 = self::getDataGenerator()->create_user();
        self::getDataGenerator()->enrol_user($student1->id, $course->id, $student->id);
        // Setup an allocation form.
        $params = [
            'course' => $course->id,
            'state' => helper::STATE_READY,
            'numberofchoices' => 1,
            'notwant' => 0,
            'startdate' => time() - DAYSECS,
            'deadline' => time() + DAYSECS,
        ];
        $allocation = $generator->create_instance($params);
        $option1 = $generator->create_option($allocation);
        $generator->create_option($allocation);
        $generator->create_restriction($allocation, $student1, $option1);

        self::setUser($student1);
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = [
            'id' => $allocation->id,
            'choices' => [
                'choice1' => $option1->id,
            ],
        ];
        $result = external_api::call_external_function('mod_allocationform_choices_submit', $args);

        $expectedresult = [
            'status' => false,
            'warnings' => [
                [
                    'item' => 'allocationform_option',
                    'itemid' => $option1->id,
                    'warningcode' => 'choiceinvalid',
                    'message' => 'The choice is not valid for the form',
                ],
            ],
        ];
        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
    }

    /**
     * Tests picking an option more than one time.
     */
    public function test_pick_duplicate() {
        global $DB, $USER;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $student = $DB->get_record('role', ['shortname' => 'student']);
        $course = self::getDataGenerator()->create_course();
        $student1 = self::getDataGenerator()->create_user();
        self::getDataGenerator()->enrol_user($student1->id, $course->id, $student->id);
        // Setup an allocation form.
        $params = [
            'course' => $course->id,
            'state' => helper::STATE_READY,
            'numberofchoices' => 2,
            'notwant' => 0,
            'startdate' => time() - DAYSECS,
            'deadline' => time() + DAYSECS,
        ];
        $allocation = $generator->create_instance($params);
        $option1 = $generator->create_option($allocation);
        $generator->create_option($allocation);

        self::setUser($student1);
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = [
            'id' => $allocation->id,
            'choices' => [
                'choice1' => $option1->id,
                'choice2' => $option1->id,
            ],
        ];
        $result = external_api::call_external_function('mod_allocationform_choices_submit', $args);

        $expectedresult = [
            'status' => false,
            'warnings' => [
                [
                    'item' => 'allocationform_option',
                    'itemid' => $option1->id,
                    'warningcode' => 'choicealreadyselected',
                    'message' => 'The choice has been selected multiple times',
                ],
            ],
        ];
        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
    }

    /**
     * Tests picking too many options.
     */
    public function test_too_many_choices() {
        global $DB, $USER;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $student = $DB->get_record('role', ['shortname' => 'student']);
        $course = self::getDataGenerator()->create_course();
        $student1 = self::getDataGenerator()->create_user();
        self::getDataGenerator()->enrol_user($student1->id, $course->id, $student->id);
        // Setup an allocation form.
        $params = [
            'course' => $course->id,
            'state' => helper::STATE_READY,
            'numberofchoices' => 1,
            'notwant' => 0,
            'startdate' => time() - DAYSECS,
            'deadline' => time() + DAYSECS,
        ];
        $allocation = $generator->create_instance($params);
        $option1 = $generator->create_option($allocation);
        $option2 = $generator->create_option($allocation);

        self::setUser($student1);
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = [
            'id' => $allocation->id,
            'choices' => [
                'choice1' => $option1->id,
                'choice2' => $option2->id,
            ],
        ];
        $result = external_api::call_external_function('mod_allocationform_choices_submit', $args);

        $expectedresult = [
            'status' => false,
            'warnings' => [
                [
                    'item' => 'allocationform',
                    'itemid' => $allocation->id,
                    'warningcode' => 'incorrectnumberofchoices',
                    'message' => 'The form does not have a valid number of choices',
                ],
            ],
        ];
        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
    }

    /**
     * Test submitting with the wrong role.
     */
    public function test_wrong_role() {
        global $DB, $USER;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $role = $DB->get_record('role', ['shortname' => 'editingteacher']);
        $course = self::getDataGenerator()->create_course();
        $user = self::getDataGenerator()->create_user();
        self::getDataGenerator()->enrol_user($user->id, $course->id, $role->id);
        // Setup an allocation form.
        $params = [
            'course' => $course->id,
            'state' => helper::STATE_READY,
            'numberofchoices' => 1,
            'notwant' => 0,
            'startdate' => time() - DAYSECS,
            'deadline' => time() + DAYSECS,
        ];
        $allocation = $generator->create_instance($params);
        $option1 = $generator->create_option($allocation);
        $generator->create_option($allocation);

        self::setUser($user);
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = [
            'id' => $allocation->id,
            'choices' => [
                'choice1' => $option1->id,
            ],
        ];
        $result = external_api::call_external_function('mod_allocationform_choices_submit', $args);

        $expectedresult = [
            'status' => false,
            'warnings' => [
                [
                    'item' => 'allocationform',
                    'itemid' => $allocation->id,
                    'warningcode' => 'nopermission',
                    'message' => 'The user does not have the correct permissions to be allocated to the form',
                ],
            ],
        ];
        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
    }

    /**
     * Test submitting with an option that is not wanted.
     */
    public function test_notwant() {
        global $DB, $USER;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $role = $DB->get_record('role', ['shortname' => 'student']);
        $course = self::getDataGenerator()->create_course();
        $user = self::getDataGenerator()->create_user();
        self::getDataGenerator()->enrol_user($user->id, $course->id, $role->id);
        // Setup an allocation form.
        $params = [
            'course' => $course->id,
            'state' => helper::STATE_READY,
            'numberofchoices' => 1,
            'notwant' => 1,
            'startdate' => time() - DAYSECS,
            'deadline' => time() + DAYSECS,
        ];
        $allocation = $generator->create_instance($params);
        $option1 = $generator->create_option($allocation);
        $option2 = $generator->create_option($allocation);

        self::setUser($user);
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = [
            'id' => $allocation->id,
            'choices' => [
                'choice1' => $option1->id,
                'notwant' => $option2->id,
            ],
        ];
        $result = external_api::call_external_function('mod_allocationform_choices_submit', $args);

        $expectedresult = [
            'status' => true,
            'warnings' => [],
        ];
        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
    }

    /**
     * Test submitting where the user does not wish to stop their allocation to an option.
     */
    public function test_no_notwant() {
        global $DB, $USER;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $role = $DB->get_record('role', ['shortname' => 'student']);
        $course = self::getDataGenerator()->create_course();
        $user = self::getDataGenerator()->create_user();
        self::getDataGenerator()->enrol_user($user->id, $course->id, $role->id);
        // Setup an allocation form.
        $params = [
            'course' => $course->id,
            'state' => helper::STATE_READY,
            'numberofchoices' => 1,
            'notwant' => 1,
            'startdate' => time() - DAYSECS,
            'deadline' => time() + DAYSECS,
        ];
        $allocation = $generator->create_instance($params);
        $option1 = $generator->create_option($allocation);
        $generator->create_option($allocation);

        self::setUser($user);
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = [
            'id' => $allocation->id,
            'choices' => [
                'choice1' => $option1->id,
                'notwant' => 0,
            ],
        ];
        $result = external_api::call_external_function('mod_allocationform_choices_submit', $args);

        $expectedresult = [
            'status' => true,
            'warnings' => [],
        ];
        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
    }
}
