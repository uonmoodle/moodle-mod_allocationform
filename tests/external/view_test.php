<?php
// This file is part of the Tutorial Booking activity.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tests the viewed web service.
 *
 * @package     mod_allocationform
 * @category    test
 * @copyright   2019 University of Nottingham
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_allocationform\external;

use core_external\external_api;

/**
 * Tests the allocation form mod_allocationform_view web service.
 *
 * @package     mod_allocationform
 * @category    test
 * @copyright   2019 University of Nottingham
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @runInSeparateProcess
 * @covers \mod_allocationform\external\view
 * @group mod_allocationform
 * @group uon
 */
class view_test extends \advanced_testcase {
    /**
     * Test that there are no errors when running the webservice.
     */
    public function test_view() {
        global $DB, $USER;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('mod_allocationform');
        $student = $DB->get_record('role', ['shortname' => 'student']);
        $course = self::getDataGenerator()->create_course();
        $student1 = self::getDataGenerator()->create_user();
        $student2 = self::getDataGenerator()->create_user();
        self::getDataGenerator()->enrol_user($student1->id, $course->id, $student->id); // Students.
        self::getDataGenerator()->enrol_user($student2->id, $course->id, $student->id);
        // Setup an allocation form.
        $allocationform = $generator->create_instance(['course' => $course->id]);

        self::setUser($student1);
        // Do not require a session key via POST, so that the calls will not error.
        $USER->ignoresesskey = true;

        $args = ['id' => $allocationform->id];
        $result = external_api::call_external_function('mod_allocationform_view_allocationform', $args);

        $expectedresult = [
            'status' => true,
            'warnings' => [],
        ];
        $this->assertFalse($result['error']);
        $this->assertEquals($expectedresult, $result['data']);
    }
}
