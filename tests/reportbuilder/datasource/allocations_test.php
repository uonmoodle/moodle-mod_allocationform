<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tests the choices datasource works properly.
 *
 * @package     mod_allocationform
 * @copyright   University of Nottingham, 2024
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

declare(strict_types=1);

namespace mod_allocationform\reportbuilder\datasource;

use core_reportbuilder\local\filters\date;
use core_reportbuilder\local\filters\select;
use core_reportbuilder\local\filters\text;
use mod_allocationform\helper;
use mod_allocationform\reportbuilder\formatters\allocationform;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once("{$CFG->dirroot}/reportbuilder/tests/helpers.php");

/**
 * Tests the choices datasource works properly.
 *
 * @package     mod_allocationform
 * @copyright   University of Nottingham, 2024
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @covers \mod_allocationform\reportbuilder\datasource\allocations
 * @group mod_allocationform
 * @group uon
 */
class allocations_test extends \core_reportbuilder_testcase {
    /**
     * Test default datasource
     */
    public function test_datasource_default(): void {
        $this->resetAfterTest();

        $category = $this->getDataGenerator()->create_category(['name' => 'Zoo', 'idnumber' => 'Z01']);
        $course = $this->getDataGenerator()->create_course(['category' => $category->id]);
        $user1 = $this->getDataGenerator()->create_user(['firstname' => 'Lucy', 'lastname' => 'Lovelace']);
        $user2 = $this->getDataGenerator()->create_user(['firstname' => 'Bob', 'lastname' => 'Builder']);
        $this->getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        $this->getDataGenerator()->enrol_user($user2->id, $course->id, 'student');

        // Create an allocation form and get the users to have made some choices and allocations.
        $allocationparams = [
            'course' => $course->id,
            'state' => helper::STATE_PROCESSED,
            'numberofchoices' => 1,
            'numberofallocations' => 1,
            'notwant' => 0,
        ];
        $allocation = $this->getDataGenerator()->create_module('allocationform', $allocationparams);

        /** @var \mod_allocationform_generator $formgenerator */
        $formgenerator = $this->getDataGenerator()->get_plugin_generator('mod_allocationform');
        $option1 = $formgenerator->create_option($allocation);
        $option2 = $formgenerator->create_option($allocation);
        $formgenerator->create_option($allocation);
        $formgenerator->create_user_choices(
            $allocation,
            $user1,
            [
                'choice1' => $option1->id,
            ]
        );
        $formgenerator->create_user_choices(
            $allocation,
            $user2,
            [
                'choice1' => $option2->id,
            ]
        );
        $formgenerator->create_allocation($allocation, $user1, $option1);
        $formgenerator->create_allocation($allocation, $user2, $option2);

        /** @var \core_reportbuilder_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('core_reportbuilder');
        $report = $generator->create_report(['name' => 'My report', 'source' => allocations::class, 'default' => 1]);

        $content = $this->get_custom_report_content($report->get('id'));
        $this->assertCount(2, $content);

        $formnamelink = allocationform::name($allocation->name, $allocation);

        // Default columns are form name with link, user's full name, and their allocation.
        // Sorted by form name, then user's full name ascending.
        $expectedrow1 = [
            $formnamelink,
            fullname($user2),
            $option2->name,
        ];
        $expectedrow2 = [
            $formnamelink,
            fullname($user1),
            $option1->name,
        ];
        $this->assertEquals([
            $expectedrow1,
            $expectedrow2,
        ], array_map('array_values', $content));
    }

    /**
     * Test datasource columns that aren't added by default.
     *
     * We are only going to ensure that one column from each entity is included here,
     * as we are testing the other entities more fully else where, including the choices data source test.
     *
     * So the aim here is to make sure all the entities play nicely together.
     */
    public function test_datasource_non_default_columns(): void {
        $this->resetAfterTest();

        $category = $this->getDataGenerator()->create_category(['name' => 'Zoo', 'idnumber' => 'Z01']);
        $course = $this->getDataGenerator()->create_course(['category' => $category->id]);
        $user1 = $this->getDataGenerator()->create_user(['firstname' => 'Lucy', 'lastname' => 'Lovelace']);

        // Create an allocation form and get the users to have made some choices and allocations.
        $allocationparams = [
            'course' => $course->id,
            'state' => helper::STATE_PROCESSED,
            'numberofchoices' => 1,
            'numberofallocations' => 1,
            'notwant' => 0,
        ];
        $allocation = $this->getDataGenerator()->create_module('allocationform', $allocationparams);

        /** @var \mod_allocationform_generator $formgenerator */
        $formgenerator = $this->getDataGenerator()->get_plugin_generator('mod_allocationform');
        $option1 = $formgenerator->create_option($allocation);
        $formgenerator->create_option($allocation);
        $formgenerator->create_user_choices(
            $allocation,
            $user1,
            [
                'choice1' => $option1->id,
            ]
        );
        $formgenerator->create_allocation($allocation, $user1, $option1);

        // Create a blank report and add the additional columns.
        /** @var \core_reportbuilder_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('core_reportbuilder');
        $report = $generator->create_report(['name' => 'My report', 'source' => allocations::class, 'default' => 0]);
        $reportid = $report->get('id');

        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'course_category:name']);
        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'course:fullname']);
        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'allocationform:name']);
        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'user:lastname']);

        $content = $this->get_custom_report_content($reportid);
        $this->assertCount(1, $content);

        $expectedrow1 = [
            $category->name,
            $course->fullname,
            $allocation->name,
            $user1->lastname,
        ];
        $this->assertEquals([$expectedrow1], array_map('array_values', $content));
    }

    /**
     * Tests that the course_category entity can be used without columns from the course entity.
     *
     * This could fail if we have the joins between the entities wrong.
     */
    public function test_datasource_joins() {
        $this->resetAfterTest();

        $category = $this->getDataGenerator()->create_category(['name' => 'Zoo', 'idnumber' => 'Z01']);
        $course = $this->getDataGenerator()->create_course(['category' => $category->id]);
        $user1 = $this->getDataGenerator()->create_user(['firstname' => 'Lucy', 'lastname' => 'Lovelace']);

        // Create an allocation form and get the users to have made some choices and allocations.
        $allocationparams = [
            'course' => $course->id,
            'state' => helper::STATE_PROCESSED,
            'numberofchoices' => 1,
            'numberofallocations' => 1,
            'notwant' => 0,
        ];
        $allocation = $this->getDataGenerator()->create_module('allocationform', $allocationparams);

        /** @var \mod_allocationform_generator $formgenerator */
        $formgenerator = $this->getDataGenerator()->get_plugin_generator('mod_allocationform');
        $option1 = $formgenerator->create_option($allocation);
        $formgenerator->create_option($allocation);
        $formgenerator->create_user_choices(
            $allocation,
            $user1,
            [
                'choice1' => $option1->id,
            ]
        );
        $formgenerator->create_allocation($allocation, $user1, $option1);

        // Create a blank report and add the additional columns.
        /** @var \core_reportbuilder_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('core_reportbuilder');
        $report = $generator->create_report(['name' => 'My report', 'source' => allocations::class, 'default' => 0]);
        $reportid = $report->get('id');

        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'course_category:name']);
        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'allocationform_allocations:allocation']);

        $content = $this->get_custom_report_content($reportid);
        $this->assertCount(1, $content);

        $expectedrow1 = [
            $category->name,
            $option1->name,
        ];
        $this->assertEquals([$expectedrow1], array_map('array_values', $content));
    }

    /**
     * Test datasource filters
     *
     * The match will be in the form of an array of arrays
     * [
     *      [
     *          'User's lastname',
     *          'User's allocation',
     *      ],
     * ]
     *
     * @param string $filtername
     * @param array $filtervalues
     * @param array $expectmatch The expected results
     *
     * @dataProvider datasource_filters_provider
     */
    public function test_datasource_filters(string $filtername, array $filtervalues, array $expectmatch): void {
        $this->resetAfterTest();

        $category = $this->getDataGenerator()->create_category(['name' => 'Zoo', 'idnumber' => 'Z01']);
        $course = $this->getDataGenerator()->create_course(['category' => $category->id]);
        $user1 = $this->getDataGenerator()->create_user(['firstname' => 'Lucy', 'lastname' => 'Lovelace']);
        $user2 = $this->getDataGenerator()->create_user(['firstname' => 'Bob', 'lastname' => 'Builder']);
        $this->getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        $this->getDataGenerator()->enrol_user($user2->id, $course->id, 'student');

        // Create an allocation form and get the users to have made some choices and allocations.
        $allocationparams = [
            'name' => 'My wonderful form',
            'course' => $course->id,
            'state' => helper::STATE_PROCESSED,
            'numberofchoices' => 2,
            'numberofallocations' => 2,
            'notwant' => 0,
        ];
        $allocation = $this->getDataGenerator()->create_module('allocationform', $allocationparams);

        /** @var \mod_allocationform_generator $formgenerator */
        $formgenerator = $this->getDataGenerator()->get_plugin_generator('mod_allocationform');
        $option1 = $formgenerator->create_option($allocation, ['name' => 'Option 1']);
        $option2 = $formgenerator->create_option($allocation, ['name' => 'Option 2']);
        $option3 = $formgenerator->create_option($allocation, ['name' => 'Option 3']);
        $formgenerator->create_option($allocation);
        $formgenerator->create_user_choices(
            $allocation,
            $user1,
            [
                'choice1' => $option1->id,
                'choice2' => $option2->id,
            ]
        );
        $formgenerator->create_user_choices(
            $allocation,
            $user2,
            [
                'choice1' => $option2->id,
                'choice2' => $option3->id,
            ]
        );
        $formgenerator->create_allocation($allocation, $user1, $option1);
        $formgenerator->create_allocation($allocation, $user1, $option2);
        $formgenerator->create_allocation($allocation, $user2, $option2);
        $formgenerator->create_allocation($allocation, $user2, $option3);

        /** @var \core_reportbuilder_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('core_reportbuilder');

        // Create report containing single idnumber column, and given filter.
        $report = $generator->create_report(['name' => 'My report', 'source' => allocations::class, 'default' => 0]);
        $reportid = $report->get('id');

        $generator->create_column(
            [
                'reportid' => $reportid,
                'uniqueidentifier' => 'user:lastname',
                'sortenabled' => true,
                'sortdirection' => SORT_ASC,
                'sortorder' => 1,
            ]
        );
        $generator->create_column(
            [
                'reportid' => $reportid,
                'uniqueidentifier' => 'allocationform_allocations:allocation',
                'sortenabled' => true,
                'sortdirection' => SORT_ASC,
                'sortorder' => 2,
            ]
        );

        // Add filter, set it's values.
        $generator->create_filter(['reportid' => $reportid, 'uniqueidentifier' => $filtername]);
        $content = $this->get_custom_report_content($reportid, 0, $filtervalues);

        $this->assertEquals($expectmatch, array_map('array_values', $content));
    }

    /**
     * Data provider for {@see test_datasource_filters}
     *
     * @return array[]
     */
    public static function datasource_filters_provider(): array {
        return [
            // Allocation.
            'Filter allocation (no match)' => [
                'allocationform_allocations:allocation',
                [
                    'allocationform_allocations:allocation_operator' => text::IS_EQUAL_TO,
                    'allocationform_allocations:allocation_value' => 'Option 4',
                ],
                [],
            ],
            'Filter allocation' => [
                'allocationform_allocations:allocation',
                [
                    'allocationform_allocations:allocation_operator' => text::IS_EQUAL_TO,
                    'allocationform_allocations:allocation_value' => 'Option 2',
                ],
                [
                    ['Builder', 'Option 2'],
                    ['Lovelace', 'Option 2'],
                ],
            ],
            // Allocation form name.
            'Filter allocation form name (no match)' => [
                'allocationform:name',
                [
                    'allocationform:name_operator' => text::IS_EQUAL_TO,
                    'allocationform:name_value' => '-1',
                ],
                [],
            ],
            'Filter allocation form name' => [
                'allocationform:name',
                [
                    'allocationform:name_operator' => text::IS_EQUAL_TO,
                    'allocationform:name_value' => 'My wonderful form',
                ],
                [
                    ['Builder', 'Option 2'],
                    ['Builder', 'Option 3'],
                    ['Lovelace', 'Option 1'],
                    ['Lovelace', 'Option 2'],
                ],
            ],
            // Allocation form start date.
            'Filter allocation form start date (no match)' => [
                'allocationform:startdate',
                [
                    'allocationform:startdate_operator' => date::DATE_FUTURE,
                ],
                [],
            ],
            'Filter allocation form start date' => [
                'allocationform:startdate',
                [
                    'allocationform:startdate_operator' => date::DATE_CURRENT,
                ],
                [
                    ['Builder', 'Option 2'],
                    ['Builder', 'Option 3'],
                    ['Lovelace', 'Option 1'],
                    ['Lovelace', 'Option 2'],
                ],
            ],
            // Allocation form deadline.
            'Filter allocation form deadline (no match)' => [
                'allocationform:deadline',
                [
                    'allocationform:deadline_operator' => date::DATE_PAST,
                ],
                [],
            ],
            'Filter allocation form deadline' => [
                'allocationform:deadline',
                [
                    'allocationform:deadline_operator' => date::DATE_FUTURE,
                ],
                [
                    ['Builder', 'Option 2'],
                    ['Builder', 'Option 3'],
                    ['Lovelace', 'Option 1'],
                    ['Lovelace', 'Option 2'],
                ],
            ],
            // Allocation form processing date.
            'Filter allocation form processed date (no match)' => [
                'allocationform:processed',
                [
                    'allocationform:processed_operator' => date::DATE_NOT_EMPTY,
                ],
                [],
            ],
            'Filter allocation form processed date' => [
                'allocationform:processed',
                [
                    'allocationform:processed_operator' => date::DATE_EMPTY,
                ],
                [
                    ['Builder', 'Option 2'],
                    ['Builder', 'Option 3'],
                    ['Lovelace', 'Option 1'],
                    ['Lovelace', 'Option 2'],
                ],
            ],
            // Allocation form sate.
            'Filter allocation form state (no match)' => [
                'allocationform:state',
                [
                    'allocationform:state_operator' => select::NOT_EQUAL_TO,
                    'allocationform:state_value' => helper::STATE_PROCESSED,
                ],
                [],
            ],
            'Filter allocation form state' => [
                'allocationform:state',
                [
                    'allocationform:state_operator' => select::EQUAL_TO,
                    'allocationform:state_value' => helper::STATE_PROCESSED,
                ],
                [
                    ['Builder', 'Option 2'],
                    ['Builder', 'Option 3'],
                    ['Lovelace', 'Option 1'],
                    ['Lovelace', 'Option 2'],
                ],
            ],
            // By user.
            'Filter user (no match)' => [
                'user:lastname',
                [
                    'user:lastname_operator' => select::EQUAL_TO,
                    'user:lastname_value' => 'Johnson',
                ],
                [],
            ],
            'Filter user' => [
                'user:lastname',
                [
                    'user:lastname_operator' => select::EQUAL_TO,
                    'user:lastname_value' => 'Builder',
                ],
                [
                    ['Builder', 'Option 2'],
                    ['Builder', 'Option 3'],
                ],
            ],
        ];
    }

    /**
     * Stress test datasource
     *
     * In order to execute this test PHPUNIT_LONGTEST should be defined as true in phpunit.xml or directly in config.php
     */
    public function test_stress_datasource(): void {
        if (!PHPUNIT_LONGTEST) {
            $this->markTestSkipped('PHPUNIT_LONGTEST is not defined');
        }

        $this->resetAfterTest();

        $category = $this->getDataGenerator()->create_category(['name' => 'Zoo', 'idnumber' => 'Z01']);
        $course = $this->getDataGenerator()->create_course(['category' => $category->id]);
        $user1 = $this->getDataGenerator()->create_user(['firstname' => 'Lucy', 'lastname' => 'Lovelace']);
        $user2 = $this->getDataGenerator()->create_user(['firstname' => 'Bob', 'lastname' => 'Builder']);
        $this->getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        $this->getDataGenerator()->enrol_user($user2->id, $course->id, 'student');

        // Create an allocation form and get the users to have made some choices and allocations.
        $allocationparams = [
            'course' => $course->id,
            'state' => helper::STATE_PROCESSED,
            'numberofchoices' => 1,
            'numberofallocations' => 1,
            'notwant' => 0,
        ];
        $allocation = $this->getDataGenerator()->create_module('allocationform', $allocationparams);

        /** @var \mod_allocationform_generator $formgenerator */
        $formgenerator = $this->getDataGenerator()->get_plugin_generator('mod_allocationform');
        $option1 = $formgenerator->create_option($allocation);
        $option2 = $formgenerator->create_option($allocation);
        $formgenerator->create_option($allocation);
        $formgenerator->create_user_choices(
            $allocation,
            $user1,
            [
                'choice1' => $option1->id,
            ]
        );
        $formgenerator->create_user_choices(
            $allocation,
            $user2,
            [
                'choice1' => $option2->id,
            ]
        );
        $formgenerator->create_allocation($allocation, $user1, $option1);
        $formgenerator->create_allocation($allocation, $user2, $option2);

        $this->datasource_stress_test_columns(allocations::class);
        $this->datasource_stress_test_columns_aggregation(allocations::class);
        $this->datasource_stress_test_conditions(allocations::class, 'allocationform:name');
    }
}
