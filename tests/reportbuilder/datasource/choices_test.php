<?php
// This file is part of the Allocation form activity module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tests the choices datasource works properly.
 *
 * @package     mod_allocationform
 * @copyright   University of Nottingham, 2024
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

declare(strict_types=1);

namespace mod_allocationform\reportbuilder\datasource;

use core_reportbuilder\local\filters\date;
use core_reportbuilder\local\filters\select;
use core_reportbuilder\local\filters\text;
use core_reportbuilder\local\helpers\format;
use mod_allocationform\helper;
use mod_allocationform\reportbuilder\formatters\allocationform;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once("{$CFG->dirroot}/reportbuilder/tests/helpers.php");

/**
 * Tests the choices datasource works properly.
 *
 * @package     mod_allocationform
 * @copyright   University of Nottingham, 2024
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @covers \mod_allocationform\reportbuilder\datasource\choices
 * @group mod_allocationform
 * @group uon
 */
class choices_test extends \core_reportbuilder_testcase {
    /**
     * Test default datasource
     */
    public function test_datasource_default(): void {
        $this->resetAfterTest();

        $category = $this->getDataGenerator()->create_category(['name' => 'Zoo', 'idnumber' => 'Z01']);
        $course = $this->getDataGenerator()->create_course(['category' => $category->id]);
        $user1 = $this->getDataGenerator()->create_user(['firstname' => 'Lucy', 'lastname' => 'Lovelace']);
        $user2 = $this->getDataGenerator()->create_user(['firstname' => 'Bob', 'lastname' => 'Builder']);
        $this->getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        $this->getDataGenerator()->enrol_user($user2->id, $course->id, 'student');

        // Create an allocation form and get the users to have made some choices.
        $allocationparams = [
            'course' => $course->id,
            'state' => helper::STATE_READY,
            'numberofchoices' => 10,
            'notwant' => 1,
        ];
        $allocation = $this->getDataGenerator()->create_module('allocationform', $allocationparams);

        /** @var \mod_allocationform_generator $formgenerator */
        $formgenerator = $this->getDataGenerator()->get_plugin_generator('mod_allocationform');
        $option1 = $formgenerator->create_option($allocation);
        $option2 = $formgenerator->create_option($allocation);
        $option3 = $formgenerator->create_option($allocation);
        $option4 = $formgenerator->create_option($allocation);
        $option5 = $formgenerator->create_option($allocation);
        $option6 = $formgenerator->create_option($allocation);
        $option7 = $formgenerator->create_option($allocation);
        $option8 = $formgenerator->create_option($allocation);
        $option9 = $formgenerator->create_option($allocation);
        $option10 = $formgenerator->create_option($allocation);
        $option11 = $formgenerator->create_option($allocation);
        $formgenerator->create_user_choices(
            $allocation,
            $user1,
            [
                'choice1' => $option1->id,
                'choice2' => $option2->id,
                'choice3' => $option3->id,
                'choice4' => $option4->id,
                'choice5' => $option5->id,
                'choice6' => $option6->id,
                'choice7' => $option7->id,
                'choice8' => $option8->id,
                'choice9' => $option9->id,
                'choice10' => $option10->id,
                'notwant' => $option11->id,
            ]
        );
        $formgenerator->create_user_choices(
            $allocation,
            $user2,
            [
                'choice1' => $option11->id,
                'choice2' => $option10->id,
                'choice3' => $option9->id,
                'choice4' => $option8->id,
                'choice5' => $option7->id,
                'choice6' => $option6->id,
                'choice7' => $option5->id,
                'choice8' => $option4->id,
                'choice9' => $option3->id,
                'choice10' => $option2->id,
                'notwant' => $option1->id,
            ]
        );

        /** @var \core_reportbuilder_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('core_reportbuilder');
        $report = $generator->create_report(['name' => 'My report', 'source' => choices::class, 'default' => 1]);

        $content = $this->get_custom_report_content($report->get('id'));
        $this->assertCount(2, $content);

        $formnamelink = allocationform::name($allocation->name, $allocation);

        // Default columns are form name with link, user's full name, all their choices numbers 1 to 10,
        // then the not want option. Sorted by form name, then user's full name ascending.
        $expectedrow1 = [
            $formnamelink,
            fullname($user2),
            $option11->name,
            $option10->name,
            $option9->name,
            $option8->name,
            $option7->name,
            $option6->name,
            $option5->name,
            $option4->name,
            $option3->name,
            $option2->name,
            $option1->name,
        ];
        $expectedrow2 = [
            $formnamelink,
            fullname($user1),
            $option1->name,
            $option2->name,
            $option3->name,
            $option4->name,
            $option5->name,
            $option6->name,
            $option7->name,
            $option8->name,
            $option9->name,
            $option10->name,
            $option11->name,
        ];
        $this->assertEquals([
            $expectedrow1,
            $expectedrow2,
        ], array_map('array_values', $content));
    }

    /**
     * Test datasource columns that aren't added by default.
     *
     * We are only going to test the all columns from entities created by this plugin.
     *
     * In practice that means the allocationform entity, since all the allocationform_choices columns
     * are in the reports by default.
     *
     * For the other entities we will only test a single column to ensure that they are being joined correctly.
     *
     * This will be the course and course_category entities.
     */
    public function test_datasource_non_default_columns(): void {
        $this->resetAfterTest();

        $category = $this->getDataGenerator()->create_category(['name' => 'Zoo', 'idnumber' => 'Z01']);
        $course = $this->getDataGenerator()->create_course(['category' => $category->id]);
        $user1 = $this->getDataGenerator()->create_user(['firstname' => 'Lucy', 'lastname' => 'Lovelace']);

        // Create an allocation form and get the user to have made a choice.
        $allocationparams = [
            'course' => $course->id,
            'state' => helper::STATE_READY,
            'numberofchoices' => 1,
            'notwant' => 0,
        ];
        $allocation = $this->getDataGenerator()->create_module('allocationform', $allocationparams);

        /** @var \mod_allocationform_generator $formgenerator */
        $formgenerator = $this->getDataGenerator()->get_plugin_generator('mod_allocationform');
        $option1 = $formgenerator->create_option($allocation);
        $formgenerator->create_option($allocation);
        $formgenerator->create_user_choices($allocation, $user1, ['choice1' => $option1->id]);
        list($course, $cm) = get_course_and_cm_from_instance($allocation, 'allocationform', $course, $user1->id);
        $context = \core\context\module::instance($cm->id);

        $contextarray = [
            'ctxid' => $context->id,
            'ctxcontextlevel' => $context->contextlevel,
            'ctxinstanceid' => $context->instanceid,
            'ctxpath' => $context->path,
            'ctxdepth' => $context->depth,
            'ctxlocked' => $context->locked,
        ];

        // Create a blank report and add the additional columns.
        /** @var \core_reportbuilder_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('core_reportbuilder');
        $report = $generator->create_report(['name' => 'My report', 'source' => choices::class, 'default' => 0]);
        $reportid = $report->get('id');

        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'course_category:name']);
        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'course:fullname']);
        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'allocationform:name']);
        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'allocationform:intro']);
        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'allocationform:options']);
        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'allocationform:startdate']);
        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'allocationform:deadline']);
        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'allocationform:processed']);
        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'allocationform:state']);
        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'user:lastname']);

        $content = $this->get_custom_report_content($reportid);
        $this->assertCount(1, $content);

        // Fake the data that will be passed to the formatting functions.
        $data = (object)array_merge($contextarray, (array)$allocation);

        $expectedrow1 = [
            $category->name,
            $course->fullname,
            $allocation->name,
            allocationform::intro($allocation->intro, $data),
            allocationform::options('', $allocation),
            format::userdate((int) $allocation->startdate, $data),
            format::userdate((int) $allocation->deadline, $data),
            format::userdate((int) $allocation->processed, $data),
            allocationform::state($allocation->state),
            $user1->lastname,
        ];
        $this->assertEquals([$expectedrow1], array_map('array_values', $content));
    }

    /**
     * Tests that the course_category entity can be used without columns from the course entity.
     *
     * This could fail if we have the joins between the entities wrong.
     */
    public function test_datasource_joins() {
        $this->resetAfterTest();

        $category = $this->getDataGenerator()->create_category(['name' => 'Zoo', 'idnumber' => 'Z01']);
        $course = $this->getDataGenerator()->create_course(['category' => $category->id]);
        $user1 = $this->getDataGenerator()->create_user(['firstname' => 'Lucy', 'lastname' => 'Lovelace']);

        // Create an allocation form and get the user to have made a choice.
        $allocationparams = [
            'course' => $course->id,
            'state' => helper::STATE_READY,
            'numberofchoices' => 1,
            'notwant' => 0,
        ];
        $allocation = $this->getDataGenerator()->create_module('allocationform', $allocationparams);

        /** @var \mod_allocationform_generator $formgenerator */
        $formgenerator = $this->getDataGenerator()->get_plugin_generator('mod_allocationform');
        $option1 = $formgenerator->create_option($allocation);
        $formgenerator->create_option($allocation);
        $formgenerator->create_user_choices($allocation, $user1, ['choice1' => $option1->id]);

        // Create a blank report and add the additional columns.
        /** @var \core_reportbuilder_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('core_reportbuilder');
        $report = $generator->create_report(['name' => 'My report', 'source' => choices::class, 'default' => 0]);
        $reportid = $report->get('id');

        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'course_category:name']);
        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'allocationform_choices:choice1']);

        $content = $this->get_custom_report_content($reportid);
        $this->assertCount(1, $content);

        $expectedrow1 = [
            $category->name,
            $option1->name,
        ];
        $this->assertEquals([$expectedrow1], array_map('array_values', $content));
    }

    /**
     * Test datasource filters
     *
     * The match will be in the form of an array of arrays
     * [
     *      [
     *          'User's lastname',
     *          'User's first choice',
     *      ],
     * ]
     *
     * @param string $filtername
     * @param array $filtervalues
     * @param array $expectmatch The expected results
     *
     * @dataProvider datasource_filters_provider
     */
    public function test_datasource_filters(string $filtername, array $filtervalues, array $expectmatch): void {
        $this->resetAfterTest();

        $category = $this->getDataGenerator()->create_category(['name' => 'Zoo', 'idnumber' => 'Z01']);
        $course = $this->getDataGenerator()->create_course(['category' => $category->id]);
        $user1 = $this->getDataGenerator()->create_user(['firstname' => 'Lucy', 'lastname' => 'Lovelace']);
        $user2 = $this->getDataGenerator()->create_user(['firstname' => 'Bob', 'lastname' => 'Builder']);
        $this->getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        $this->getDataGenerator()->enrol_user($user2->id, $course->id, 'student');

        // Create an allocation form and get the users to have made some choices.
        $allocationparams = [
            'course' => $course->id,
            'state' => helper::STATE_READY,
            'name' => 'My wonderful form',
            'numberofchoices' => 10,
            'notwant' => 1,
        ];
        $allocation = $this->getDataGenerator()->create_module('allocationform', $allocationparams);

        /** @var \mod_allocationform_generator $formgenerator */
        $formgenerator = $this->getDataGenerator()->get_plugin_generator('mod_allocationform');
        $option1 = $formgenerator->create_option($allocation, ['name' => 'Option 1']);
        $option2 = $formgenerator->create_option($allocation, ['name' => 'Option 2']);
        $option3 = $formgenerator->create_option($allocation, ['name' => 'Option 3']);
        $option4 = $formgenerator->create_option($allocation, ['name' => 'Option 4']);
        $option5 = $formgenerator->create_option($allocation, ['name' => 'Option 5']);
        $option6 = $formgenerator->create_option($allocation, ['name' => 'Option 6']);
        $option7 = $formgenerator->create_option($allocation, ['name' => 'Option 7']);
        $option8 = $formgenerator->create_option($allocation, ['name' => 'Option 8']);
        $option9 = $formgenerator->create_option($allocation, ['name' => 'Option 9']);
        $option10 = $formgenerator->create_option($allocation, ['name' => 'Option 10']);
        $option11 = $formgenerator->create_option($allocation, ['name' => 'Option 11']);
        $formgenerator->create_user_choices(
            $allocation,
            $user1,
            [
                'choice1' => $option1->id,
                'choice2' => $option2->id,
                'choice3' => $option3->id,
                'choice4' => $option4->id,
                'choice5' => $option5->id,
                'choice6' => $option6->id,
                'choice7' => $option7->id,
                'choice8' => $option8->id,
                'choice9' => $option9->id,
                'choice10' => $option10->id,
                'notwant' => $option11->id,
            ]
        );
        $formgenerator->create_user_choices(
            $allocation,
            $user2,
            [
                'choice1' => $option11->id,
                'choice2' => $option10->id,
                'choice3' => $option9->id,
                'choice4' => $option8->id,
                'choice5' => $option7->id,
                'choice6' => $option6->id,
                'choice7' => $option5->id,
                'choice8' => $option4->id,
                'choice9' => $option3->id,
                'choice10' => $option2->id,
                'notwant' => $option1->id,
            ]
        );

        /** @var \core_reportbuilder_generator $generator */
        $generator = $this->getDataGenerator()->get_plugin_generator('core_reportbuilder');

        // Create report containing single idnumber column, and given filter.
        $report = $generator->create_report(['name' => 'My report', 'source' => choices::class, 'default' => 0]);
        $reportid = $report->get('id');

        $generator->create_column(
            [
                'reportid' => $reportid,
                'uniqueidentifier' => 'user:lastname',
                'sortenabled' => true,
                'sortdirection' => SORT_ASC,
                'sortorder' => 1,
            ]
        );
        $generator->create_column(['reportid' => $reportid, 'uniqueidentifier' => 'allocationform_choices:choice1']);

        // Add filter, set it's values.
        $generator->create_filter(['reportid' => $reportid, 'uniqueidentifier' => $filtername]);
        $content = $this->get_custom_report_content($reportid, 0, $filtervalues);

        $this->assertEquals($expectmatch, array_map('array_values', $content));
    }

    /**
     * Data provider for {@see test_datasource_filters}
     *
     * @return array[]
     */
    public static function datasource_filters_provider(): array {
        return [
            // Choice 1.
            'Filter choice1 (no match)' => [
                'allocationform_choices:choice1',
                [
                    'allocationform_choices:choice1_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice1_value' => 'Option 2',
                ],
                [],
            ],
            'Filter choice1' => [
                'allocationform_choices:choice1',
                [
                    'allocationform_choices:choice1_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice1_value' => 'Option 1',
                ],
                [
                    ['Lovelace', 'Option 1'],
                ],
            ],
            // Choice 2.
            'Filter choice2 (no match)' => [
                'allocationform_choices:choice2',
                [
                    'allocationform_choices:choice2_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice2_value' => 'Option 1',
                ],
                [],
            ],
            'Filter choice2' => [
                'allocationform_choices:choice2',
                [
                    'allocationform_choices:choice2_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice2_value' => 'Option 10',
                ],
                [
                    ['Builder', 'Option 11'],
                ],
            ],
            // Choice 3.
            'Filter choice3 (no match)' => [
                'allocationform_choices:choice3',
                [
                    'allocationform_choices:choice3_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice3_value' => 'Option 1',
                ],
                [],
            ],
            'Filter choice3' => [
                'allocationform_choices:choice3',
                [
                    'allocationform_choices:choice3_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice3_value' => 'Option 3',
                ],
                [
                    ['Lovelace', 'Option 1'],
                ],
            ],
            // Choice 4.
            'Filter choice4 (no match)' => [
                'allocationform_choices:choice4',
                [
                    'allocationform_choices:choice4_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice4_value' => 'Option 1',
                ],
                [],
            ],
            'Filter choice4' => [
                'allocationform_choices:choice4',
                [
                    'allocationform_choices:choice4_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice4_value' => 'Option 4',
                ],
                [
                    ['Lovelace', 'Option 1'],
                ],
            ],
            // Choice 5.
            'Filter choice5 (no match)' => [
                'allocationform_choices:choice5',
                [
                    'allocationform_choices:choice5_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice5_value' => 'Option 1',
                ],
                [],
            ],
            'Filter choice5' => [
                'allocationform_choices:choice5',
                [
                    'allocationform_choices:choice5_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice5_value' => 'Option 5',
                ],
                [
                    ['Lovelace', 'Option 1'],
                ],
            ],
            // Choice 6.
            'Filter choice6 (no match)' => [
                'allocationform_choices:choice6',
                [
                    'allocationform_choices:choice6_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice6_value' => 'Option 1',
                ],
                [],
            ],
            'Filter choice6' => [
                'allocationform_choices:choice6',
                [
                    'allocationform_choices:choice6_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice6_value' => 'Option 6',
                ],
                [
                    ['Builder', 'Option 11'],
                    ['Lovelace', 'Option 1'],
                ],
            ],
            // Choice 7.
            'Filter choice7 (no match)' => [
                'allocationform_choices:choice7',
                [
                    'allocationform_choices:choice7_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice7_value' => 'Option 1',
                ],
                [],
            ],
            'Filter choice7' => [
                'allocationform_choices:choice7',
                [
                    'allocationform_choices:choice7_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice7_value' => 'Option 7',
                ],
                [
                    ['Lovelace', 'Option 1'],
                ],
            ],
            // Choice 8.
            'Filter choice8 (no match)' => [
                'allocationform_choices:choice8',
                [
                    'allocationform_choices:choice8_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice8_value' => 'Option 1',
                ],
                [],
            ],
            'Filter choice8' => [
                'allocationform_choices:choice8',
                [
                    'allocationform_choices:choice8_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice8_value' => 'Option 8',
                ],
                [
                    ['Lovelace', 'Option 1'],
                ],
            ],
            // Choice 9.
            'Filter choice9 (no match)' => [
                'allocationform_choices:choice9',
                [
                    'allocationform_choices:choice9_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice9_value' => 'Option 1',
                ],
                [],
            ],
            'Filter choice9' => [
                'allocationform_choices:choice9',
                [
                    'allocationform_choices:choice9_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice9_value' => 'Option 9',
                ],
                [
                    ['Lovelace', 'Option 1'],
                ],
            ],
            // Choice 10.
            'Filter choice10 (no match)' => [
                'allocationform_choices:choice10',
                [
                    'allocationform_choices:choice10_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice10_value' => 'Option 1',
                ],
                [],
            ],
            'Filter choice10' => [
                'allocationform_choices:choice10',
                [
                    'allocationform_choices:choice10_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:choice10_value' => 'Option 10',
                ],
                [
                    ['Lovelace', 'Option 1'],
                ],
            ],
            // Not want choice.
            'Filter notwant (no match)' => [
                'allocationform_choices:notwant',
                [
                    'allocationform_choices:notwant_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:notwant_value' => 'Option 3',
                ],
                [],
            ],
            'Filter notwant' => [
                'allocationform_choices:notwant',
                [
                    'allocationform_choices:notwant_operator' => text::IS_EQUAL_TO,
                    'allocationform_choices:notwant_value' => 'Option 11',
                ],
                [
                    ['Lovelace', 'Option 1'],
                ],
            ],
            // Allocation form name.
            'Filter allocation form name (no match)' => [
                'allocationform:name',
                [
                    'allocationform:name_operator' => text::IS_EQUAL_TO,
                    'allocationform:name_value' => '-1',
                ],
                [],
            ],
            'Filter allocation form name' => [
                'allocationform:name',
                [
                    'allocationform:name_operator' => text::IS_EQUAL_TO,
                    'allocationform:name_value' => 'My wonderful form',
                ],
                [
                    ['Builder', 'Option 11'],
                    ['Lovelace', 'Option 1'],
                ],
            ],
            // Allocation form start date.
            'Filter allocation form start date (no match)' => [
                'allocationform:startdate',
                [
                    'allocationform:startdate_operator' => date::DATE_FUTURE,
                ],
                [],
            ],
            'Filter allocation form start date' => [
                'allocationform:startdate',
                [
                    'allocationform:startdate_operator' => date::DATE_CURRENT,
                ],
                [
                    ['Builder', 'Option 11'],
                    ['Lovelace', 'Option 1'],
                ],
            ],
            // Allocation form deadline.
            'Filter allocation form deadline (no match)' => [
                'allocationform:deadline',
                [
                    'allocationform:deadline_operator' => date::DATE_PAST,
                ],
                [],
            ],
            'Filter allocation form deadline' => [
                'allocationform:deadline',
                [
                    'allocationform:deadline_operator' => date::DATE_FUTURE,
                ],
                [
                    ['Builder', 'Option 11'],
                    ['Lovelace', 'Option 1'],
                ],
            ],
            // Allocation form processing date.
            'Filter allocation form processed date (no match)' => [
                'allocationform:processed',
                [
                    'allocationform:processed_operator' => date::DATE_NOT_EMPTY,
                ],
                [],
            ],
            'Filter allocation form processed date' => [
                'allocationform:processed',
                [
                    'allocationform:processed_operator' => date::DATE_EMPTY,
                ],
                [
                    ['Builder', 'Option 11'],
                    ['Lovelace', 'Option 1'],
                ],
            ],
            // Allocation form sate.
            'Filter allocation form state (no match)' => [
                'allocationform:state',
                [
                    'allocationform:state_operator' => select::NOT_EQUAL_TO,
                    'allocationform:state_value' => helper::STATE_READY,
                ],
                [],
            ],
            'Filter allocation form state' => [
                'allocationform:state',
                [
                    'allocationform:state_operator' => select::EQUAL_TO,
                    'allocationform:state_value' => helper::STATE_READY,
                ],
                [
                    ['Builder', 'Option 11'],
                    ['Lovelace', 'Option 1'],
                ],
            ],
        ];
    }

    /**
     * Stress test datasource
     *
     * In order to execute this test PHPUNIT_LONGTEST should be defined as true in phpunit.xml or directly in config.php
     */
    public function test_stress_datasource(): void {
        if (!PHPUNIT_LONGTEST) {
            $this->markTestSkipped('PHPUNIT_LONGTEST is not defined');
        }

        $this->resetAfterTest();

        $category = $this->getDataGenerator()->create_category(['name' => 'Zoo', 'idnumber' => 'Z01']);
        $course = $this->getDataGenerator()->create_course(['category' => $category->id]);
        $user1 = $this->getDataGenerator()->create_user(['firstname' => 'Lucy', 'lastname' => 'Lovelace']);
        $user2 = $this->getDataGenerator()->create_user(['firstname' => 'Bob', 'lastname' => 'Builder']);
        $this->getDataGenerator()->enrol_user($user1->id, $course->id, 'student');
        $this->getDataGenerator()->enrol_user($user2->id, $course->id, 'student');

        // Create an allocation form and get the users to have made some choices.
        $allocationparams = [
            'course' => $course->id,
            'state' => helper::STATE_READY,
            'name' => 'My wonderful form',
            'numberofchoices' => 10,
            'notwant' => 1,
        ];
        $allocation = $this->getDataGenerator()->create_module('allocationform', $allocationparams);

        /** @var \mod_allocationform_generator $formgenerator */
        $formgenerator = $this->getDataGenerator()->get_plugin_generator('mod_allocationform');
        $option1 = $formgenerator->create_option($allocation, ['name' => 'Option 1']);
        $option2 = $formgenerator->create_option($allocation, ['name' => 'Option 2']);
        $option3 = $formgenerator->create_option($allocation, ['name' => 'Option 3']);
        $option4 = $formgenerator->create_option($allocation, ['name' => 'Option 4']);
        $option5 = $formgenerator->create_option($allocation, ['name' => 'Option 5']);
        $option6 = $formgenerator->create_option($allocation, ['name' => 'Option 6']);
        $option7 = $formgenerator->create_option($allocation, ['name' => 'Option 7']);
        $option8 = $formgenerator->create_option($allocation, ['name' => 'Option 8']);
        $option9 = $formgenerator->create_option($allocation, ['name' => 'Option 9']);
        $option10 = $formgenerator->create_option($allocation, ['name' => 'Option 10']);
        $option11 = $formgenerator->create_option($allocation, ['name' => 'Option 11']);
        $formgenerator->create_user_choices(
            $allocation,
            $user1,
            [
                'choice1' => $option1->id,
                'choice2' => $option2->id,
                'choice3' => $option3->id,
                'choice4' => $option4->id,
                'choice5' => $option5->id,
                'choice6' => $option6->id,
                'choice7' => $option7->id,
                'choice8' => $option8->id,
                'choice9' => $option9->id,
                'choice10' => $option10->id,
                'notwant' => $option11->id,
            ]
        );
        $formgenerator->create_user_choices(
            $allocation,
            $user2,
            [
                'choice1' => $option11->id,
                'choice2' => $option10->id,
                'choice3' => $option9->id,
                'choice4' => $option8->id,
                'choice5' => $option7->id,
                'choice6' => $option6->id,
                'choice7' => $option5->id,
                'choice8' => $option4->id,
                'choice9' => $option3->id,
                'choice10' => $option2->id,
                'notwant' => $option1->id,
            ]
        );

        $this->datasource_stress_test_columns(choices::class);
        $this->datasource_stress_test_columns_aggregation(choices::class);
        $this->datasource_stress_test_conditions(choices::class, 'allocationform:name');
    }
}
